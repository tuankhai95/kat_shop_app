package class_kat;

import java.io.Serializable;

/**
 * Created by ASUS X550CA on 10/12/2016.
 */

public class ProductAttributes extends Infor  implements Serializable {
    private int ProductAttributeGroupId;

    public ProductAttributes() {
    }

    public int getProductAttributeGroupId() {
        return ProductAttributeGroupId;
    }

    public void setProductAttributeGroupId(int productAttributeGroupId) {
        ProductAttributeGroupId = productAttributeGroupId;
    }
}
