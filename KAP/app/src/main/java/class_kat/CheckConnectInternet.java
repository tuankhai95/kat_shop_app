package class_kat;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
/**
 * Created by ASUS X550CA on 9/3/2016.
 */
public class CheckConnectInternet {

    private Context _context;

    //Hàm dựng khởi tạo đối tượng
    public CheckConnectInternet(Context context) {
        this._context = context;
    }

    public boolean checkMobileInternetConn() {
        //Tạo đối tương ConnectivityManager để trả về thông tin mạng
        ConnectivityManager connectivity = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo _wifi = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        //NetworkInfo _3g = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        //Nếu đối tượng khác null
        if (connectivity != null) {
            //Nhận thông tin mạng
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo _3g = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null||_3g!=null) {
                //Tìm kiếm thiết bị đã kết nối được internet chưa
                if (info.isConnected()||_3g.isConnected()) {
                    return true;
                }
            }

        }
        return false;
    }

}
