package class_kat;

import retrofit.http.POST;

/**
 * Created by ASUS X550CA on 10/16/2016.
 */

public interface UserService {
    @POST("/me")
    User me();
}
