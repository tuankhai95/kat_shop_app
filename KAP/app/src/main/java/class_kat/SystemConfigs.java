package class_kat;

import java.io.Serializable;

/**
 * Created by ASUS X550CA on 10/12/2016.
 */

public class SystemConfigs  implements Serializable {
    private int ID;
    private String Code;
    private String ValueString;
    private int ValueInt;

    public SystemConfigs() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getValueString() {
        return ValueString;
    }

    public void setValueString(String valueString) {
        ValueString = valueString;
    }

    public int getValueInt() {
        return ValueInt;
    }

    public void setValueInt(int valueInt) {
        ValueInt = valueInt;
    }
}
