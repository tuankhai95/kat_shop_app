package class_kat;

/**
 * Created by ASUS X550CA on 11/3/2016.
 */

public class CustomMyOrders {
    public String Id;
    public String date;
    public String status;


    public CustomMyOrders() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
