package class_kat;

import java.io.Serializable;

/**
 * Created by ASUS X550CA on 10/12/2016.
 */

public class ProductTemplateAttributes  implements Serializable {
    private int ProductTemplateId;
    private int ProductAttributeId;

    public ProductTemplateAttributes() {
    }

    public int getProductTemplateId() {
        return ProductTemplateId;
    }

    public void setProductTemplateId(int productTemplateId) {
        ProductTemplateId = productTemplateId;
    }

    public int getProductAttributeId() {
        return ProductAttributeId;
    }

    public void setProductAttributeId(int productAttributeId) {
        ProductAttributeId = productAttributeId;
    }
}
