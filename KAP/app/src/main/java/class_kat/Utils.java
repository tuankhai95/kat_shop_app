package class_kat;

import android.net.Uri;
import android.provider.SyncStateContract;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by ASUS X550CA on 10/29/2016.
 */

public class Utils {
    public static String buildPostParameters(Object content) {
        String output = null;
        if ((content instanceof String) || (content instanceof JSONObject) || (content instanceof JSONArray)) {
            output = content.toString();
        } else if (content instanceof Map) {
            Uri.Builder builder = new Uri.Builder();
            HashMap hashMap = (HashMap) content;
            if (hashMap != null) {
                Iterator entries = hashMap.entrySet().iterator();
                while (entries.hasNext()) {
                    Map.Entry entry = (Map.Entry) entries.next();
                    builder.appendQueryParameter(entry.getKey().toString(), entry.getValue().toString());
                    entries.remove(); // avoids a ConcurrentModificationException
                }
                output = builder.build().getEncodedQuery();
            }
        }

        return output;
    }

    public static URLConnection makeRequest(String method, String apiAddress, String accessToken, String mimeType, String requestBody) throws IOException {
        URL url = new URL(apiAddress);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(!method.equals("GET"));
        urlConnection.setRequestMethod(method);

        urlConnection.setRequestProperty("Authorization", "bearer " + accessToken);

        urlConnection.setRequestProperty("Content-Type", mimeType);
        OutputStream outputStream = new BufferedOutputStream(urlConnection.getOutputStream());
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "utf-8"));
        writer.write(requestBody);
        writer.flush();
        writer.close();
        outputStream.close();

        urlConnection.connect();

        return urlConnection;
    }
    public String putDataToServer(String url,JSONObject returnedJObject,String ass) throws Throwable {
        HttpPost request = new HttpPost(url);
        JSONStringer json = new JSONStringer();
        StringBuilder sb=new StringBuilder();


        if (returnedJObject!=null)
        {
            Iterator<String> itKeys = returnedJObject.keys();
            if(itKeys.hasNext())
                json.object();
            while (itKeys.hasNext())
            {
                String k=itKeys.next();
                json.key(k).value(returnedJObject.get(k));
                Log.e("keys "+k,"value "+returnedJObject.get(k).toString());
            }
        }
        json.endObject();


        StringEntity entity = new StringEntity(json.toString());
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,"application/json;charset=UTF-8"));
        request.addHeader("Authorization","bearer "+ass);
        request.setEntity(entity);

        HttpResponse response =null;
        DefaultHttpClient httpClient = new DefaultHttpClient();

        HttpConnectionParams.setSoTimeout(httpClient.getParams(), 1000);
        HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 1000);
        try{
            response = httpClient.execute(request);
        }
        catch(SocketException se)
        {
            Log.e("SocketException", se+"");
            throw se;
        }
        InputStream in = response.getEntity().getContent();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line = null;
        while((line = reader.readLine()) != null){
            sb.append(line);
        }
        return sb.toString();
    }
}
