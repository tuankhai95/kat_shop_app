package class_kat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ASUS X550CA on 10/7/2016.
 */

public class Product extends Infor implements Serializable{
    private   String Alias;
    private  int CategoryID;
    private int BrandID;
    private String ImageProduct;
    private String MoreImages;
    private  long Price;
    private long OriginalPrice;
    private  long PromotionPrice;
    private  int Quantity;
    private  int Warranty;
    private String Description;
    private String Content;
    private String Tags;
    private String  NormalizedName;
    private Date CreatedDate;
    private  boolean HomeFlag;
    private  boolean HotFlag;
    private  boolean IsPublished;
    private int ViewCount;
    private boolean IsDelete;
    private productBrand brand;
    private productCategory category;
    private OrderDetail orderDetail;

    public Product() {
    }

    public String getAlias() {
        return Alias;
    }

    public void setAlias(String alias) {
        Alias = alias;
    }

    public int getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(int categoryID) {
        CategoryID = categoryID;
    }

    public int getBrandID() {
        return BrandID;
    }

    public void setBrandID(int brandID) {
        BrandID = brandID;
    }

    public String getImageProduct() {
        return ImageProduct;
    }

    public void setImageProduct(String imageProduct) {
        ImageProduct = imageProduct;
    }

    public String getMoreImages() {
        return MoreImages;
    }

    public void setMoreImages(String moreImages) {
        MoreImages = moreImages;
    }

    public long getPrice() {
        return Price;
    }

    public void setPrice(long price) {
        Price = price;
    }

    public long getOriginalPrice() {
        return OriginalPrice;
    }

    public void setOriginalPrice(long originalPrice) {
        OriginalPrice = originalPrice;
    }

    public long getPromotionPrice() {
        return PromotionPrice;
    }

    public void setPromotionPrice(long promotionPrice) {
        PromotionPrice = promotionPrice;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public int getWarranty() {
        return Warranty;
    }

    public void setWarranty(int warranty) {
        Warranty = warranty;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getTags() {
        return Tags;
    }

    public void setTags(String tags) {
        Tags = tags;
    }

    public String getNormalizedName() {
        return NormalizedName;
    }

    public void setNormalizedName(String normalizedName) {
        NormalizedName = normalizedName;
    }

    public Date getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(Date createdDate) {
        CreatedDate = createdDate;
    }

    public boolean isHomeFlag() {
        return HomeFlag;
    }

    public void setHomeFlag(boolean homeFlag) {
        HomeFlag = homeFlag;
    }

    public boolean isHotFlag() {
        return HotFlag;
    }

    public void setHotFlag(boolean hotFlag) {
        HotFlag = hotFlag;
    }

    public boolean isPublished() {
        return IsPublished;
    }

    public void setPublished(boolean published) {
        IsPublished = published;
    }

    public int getViewCount() {
        return ViewCount;
    }

    public void setViewCount(int viewCount) {
        ViewCount = viewCount;
    }

    public boolean isDelete() {
        return IsDelete;
    }

    public void setDelete(boolean delete) {
        IsDelete = delete;
    }

    public productBrand getBrand() {
        return brand;
    }

    public void setBrand(productBrand brand) {
        this.brand = brand;
    }

    public productCategory getCategory() {
        return category;
    }

    public void setCategory(productCategory category) {
        this.category = category;
    }

    public OrderDetail getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(OrderDetail orderDetail) {
        this.orderDetail = orderDetail;
    }
}
