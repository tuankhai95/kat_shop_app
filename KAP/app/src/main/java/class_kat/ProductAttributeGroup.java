package class_kat;

import java.io.Serializable;

/**
 * Created by ASUS X550CA on 10/12/2016.
 */

public class ProductAttributeGroup  implements Serializable {
    private int ID;
    private String Name;

    public ProductAttributeGroup() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
