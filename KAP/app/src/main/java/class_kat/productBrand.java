package class_kat;

import java.io.Serializable;

/**
 * Created by ASUS X550CA on 10/12/2016.
 */

public class productBrand extends Infor  implements Serializable {
    private String Alias;//": "acer",
    private String Description;//": "Nhãn hiệu Acer",
    private String Image;//": "https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Acer_2011.svg/2000px-Acer_2011.svg.png",
    private boolean HomeFlag;//": true,
    private String MetaKeyword;//": "Sản phẩm Acer",
    private String MetaDescription;//": null,
    private boolean IsPublished;//": true,
    private  boolean IsDelete;//": false,

    public productBrand() {
    }

    public String getAlias() {
        return Alias;
    }

    public void setAlias(String alias) {
        Alias = alias;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public boolean isHomeFlag() {
        return HomeFlag;
    }

    public void setHomeFlag(boolean homeFlag) {
        HomeFlag = homeFlag;
    }

    public String getMetaKeyword() {
        return MetaKeyword;
    }

    public void setMetaKeyword(String metaKeyword) {
        MetaKeyword = metaKeyword;
    }

    public String getMetaDescription() {
        return MetaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        MetaDescription = metaDescription;
    }

    public boolean isPublished() {
        return IsPublished;
    }

    public void setPublished(boolean published) {
        IsPublished = published;
    }

    public boolean isDelete() {
        return IsDelete;
    }

    public void setDelete(boolean delete) {
        IsDelete = delete;
    }
}
