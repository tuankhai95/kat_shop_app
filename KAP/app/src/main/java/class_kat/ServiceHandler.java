package class_kat;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ASUS X550CA on 10/29/2016.
 */

public class ServiceHandler {
    static String response = null;
    public final static int GET = 1;
    public final static int POST = 2;

    public ServiceHandler() {

    }

    /**
     * Hàm gọi tới service
     * @url - url để gửi request
     * @method - phương thức http request
     * */

    /**
     * Hàm gọi tới service
     * @url - url để gửi request
     * @method - phương thức http request
     * @params - tham số của http request
     * */
    public static String makeServiceCall(String url, int method, String params, String accessToken) {
        try {
            // http client
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpEntity httpEntity = null;
            HttpResponse httpResponse = null;

            // Kiểm tra kiểu phương thức http request
            if (method == POST) {
                HttpPost httpPost = new HttpPost(url);
                httpPost.addHeader("Authorization","bearer "+accessToken);

                // Thêm tham số post
                if (params != null) {
                    //httpPost.setEntity(new UrlEncodedFormEntity(params));
                    url += params;
                    Log.d("urltest",url);
                }else {

                }

                httpResponse = httpClient.execute(httpPost);
                Log.d("urltest1",url);

            } else if (method == GET) {
                // Thêm tham số vào url
//                if (params != null) {
//                    String paramString = URLEncodedUtils.format(params, "utf-8");
//                    url += "?" + paramString;
//                }
                HttpGet httpGet = new HttpGet(url);

                httpResponse = httpClient.execute(httpGet);

            }
            httpEntity = httpResponse.getEntity();
            response = EntityUtils.toString(httpEntity);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("urltest2",url);
        return response;

    }
    protected String addLocationToUrl(String url){
        if(!url.endsWith("?"))
            url += "?";

        List<NameValuePair> params = new LinkedList<NameValuePair>();
        params.add(new BasicNameValuePair("lat", String.valueOf("")));
        params.add(new BasicNameValuePair("lon", String.valueOf("")));
        String paramString = URLEncodedUtils.format(params, "utf-8");

        url += paramString;
        return url;
    }
}
