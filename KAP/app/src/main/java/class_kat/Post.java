package class_kat;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by ASUS X550CA on 10/12/2016.
 */

public class Post extends Infor  implements Serializable {
    private String Alias;
    private String Description;
    private int CategoryID;
    private String Content;
    private String  Image;
    private String NormalizedName;
    private Date CreatedDate;
    private String MetaKeyword;
    private String MetaDescription;
    private boolean  HomeFlag;
    private boolean HotFlag;
    private int  ViewCount;
    private boolean IsPublished;
    private PostCategory postCategory;

    public Post() {
    }

    public String getAlias() {
        return Alias;
    }

    public void setAlias(String alias) {
        Alias = alias;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(int categoryID) {
        CategoryID = categoryID;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getNormalizedName() {
        return NormalizedName;
    }

    public void setNormalizedName(String normalizedName) {
        NormalizedName = normalizedName;
    }

    public Date getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(Date createdDate) {
        CreatedDate = createdDate;
    }

    public String getMetaKeyword() {
        return MetaKeyword;
    }

    public void setMetaKeyword(String metaKeyword) {
        MetaKeyword = metaKeyword;
    }

    public String getMetaDescription() {
        return MetaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        MetaDescription = metaDescription;
    }

    public boolean getHomeFlag() {
        return HomeFlag;
    }

    public void setHomeFlag(boolean homeFlag) {
        HomeFlag = homeFlag;
    }

    public boolean getHotFlag() {
        return HotFlag;
    }

    public void setHotFlag(boolean hotFlag) {
        HotFlag = hotFlag;
    }

    public int getViewCount() {
        return ViewCount;
    }

    public void setViewCount(int viewCount) {
        ViewCount = viewCount;
    }

    public boolean getIsPublished() {
        return IsPublished;
    }

    public void setIsPublished(boolean isPublished) {
        IsPublished = isPublished;
    }

    public boolean isHomeFlag() {
        return HomeFlag;
    }

    public boolean isHotFlag() {
        return HotFlag;
    }

    public boolean isPublished() {
        return IsPublished;
    }

    public void setPublished(boolean published) {
        IsPublished = published;
    }

    public PostCategory getPostCategory() {
        return postCategory;
    }

    public void setPostCategory(PostCategory postCategory) {
        this.postCategory = postCategory;
    }
}
