package class_kat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ASUS X550CA on 10/12/2016.
 */

public class Order  implements Serializable {
    private  int ID;
    private String CustomerName;
    private String CustomerEmail;
    private String CustomerMobile;
    private String CustomerShippingAddress;
    private String CreatedDate;
    private String CustomerMessage;
    private String CreatedBy;
    private String PaymentMethod;
    private  String PaymentStatus;
    private String CustomerBillingAddress;
    private  long SubTotal;
    private String CustomerId;
    private  int Status;
    private ArrayList<OrderDetail> arr;

    public Order() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerEmail() {
        return CustomerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        CustomerEmail = customerEmail;
    }

    public String getCustomerMobile() {
        return CustomerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        CustomerMobile = customerMobile;
    }

    public String getCustomerShippingAddress() {
        return CustomerShippingAddress;
    }

    public void setCustomerShippingAddress(String customerShippingAddress) {
        CustomerShippingAddress = customerShippingAddress;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getPaymentMethod() {
        return PaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        PaymentMethod = paymentMethod;
    }

    public String getPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public String getCustomerBillingAddress() {
        return CustomerBillingAddress;
    }

    public void setCustomerBillingAddress(String customerBillingAddress) {
        CustomerBillingAddress = customerBillingAddress;
    }

    public long getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(long subTotal) {
        SubTotal = subTotal;
    }

    public String getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(String customerId) {
        CustomerId = customerId;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getCustomerMessage() {
        return CustomerMessage;
    }

    public void setCustomerMessage(String customerMessage) {
        CustomerMessage = customerMessage;
    }

    public ArrayList<OrderDetail> getArr() {
        return arr;
    }

    public void setArr(ArrayList<OrderDetail> arr) {
        this.arr = arr;
    }
}
