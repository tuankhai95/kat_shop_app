package class_kat;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by ASUS X550CA on 10/12/2016.
 */

public class Feedback  extends Infor implements Serializable {
    private String Email;
    private String Message;
    private Date CreatedDate;

    public Feedback() {
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public Date getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(Date createdDate) {
        CreatedDate = createdDate;
    }
}
