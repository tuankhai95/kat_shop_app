package class_kat;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ASUS X550CA on 10/12/2016.
 */

public class OrderDetail  implements Serializable {

    public int Quantity;
    public long Price;
    public Order OrderId;
   private Product product;

    public OrderDetail() {
    }
    public OrderDetail(int price) {
        this.Quantity=price;
    }
    public Order getOrderId() {
        return OrderId;
    }

    public void setOrderId(Order orderId) {
        OrderId = orderId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public long getPrice() {
        return Price;
    }

    public void setPrice(long price) {
        Price = price;
    }
}
