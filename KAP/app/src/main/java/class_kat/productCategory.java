package class_kat;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ASUS X550CA on 10/12/2016.
 */

public class productCategory  extends Infor  implements Serializable {
    private  String Alias;//": "dien-thoai",
    private String Description;//": "Danh mục ngành hàng hiện thoại",
    private int ParentID;//": null,
    private int DisplayOrder;//": 1,
    private  String Image;//": "http://i.istockimg.com/file_thumbview_approve/47923056/5/stock-photo-47923056-mobile-phone.jpg",
    //private ArrayList<Product> arrayProduct;
    private String MetaKeyword;
    private String MetaDescription;
    private boolean HomeFlag;
    private  boolean IsDelete;//": false,
    private boolean IsPublished;//": true

    public productCategory() {
    }

    public String getAlias() {
        return Alias;
    }

    public void setAlias(String alias) {
        Alias = alias;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getParentID() {
        return ParentID;
    }

    public void setParentID(int parentID) {
        ParentID = parentID;
    }

    public int getDisplayOrder() {
        return DisplayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        DisplayOrder = displayOrder;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getMetaKeyword() {
        return MetaKeyword;
    }

    public void setMetaKeyword(String metaKeyword) {
        MetaKeyword = metaKeyword;
    }

    public String getMetaDescription() {
        return MetaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        MetaDescription = metaDescription;
    }

    public boolean isHomeFlag() {
        return HomeFlag;
    }

    public void setHomeFlag(boolean homeFlag) {
        HomeFlag = homeFlag;
    }

    public boolean isDelete() {
        return IsDelete;
    }

    public void setDelete(boolean delete) {
        IsDelete = delete;
    }

    public boolean isPublished() {
        return IsPublished;
    }

    public void setPublished(boolean published) {
        IsPublished = published;
    }
}
