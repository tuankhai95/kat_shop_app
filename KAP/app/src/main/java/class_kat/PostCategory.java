package class_kat;

import java.io.Serializable;

/**
 * Created by ASUS X550CA on 10/12/2016.
 */

public class PostCategory extends Infor  implements Serializable {

    private String Alias;
    private String Description;
    private int ParentID;
    private int DisplayOrder;
    private String Image;
    private String NormalizedName;
    private String MetaKeyword;
    private String MetaDescription;
    private boolean HomeFlag;
    private boolean IsPublished;

    public PostCategory() {
    }


    public String getAlias() {
        return Alias;
    }

    public void setAlias(String alias) {
        Alias = alias;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getParentID() {
        return ParentID;
    }

    public void setParentID(int parentID) {
        ParentID = parentID;
    }

    public int getDisplayOrder() {
        return DisplayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        DisplayOrder = displayOrder;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getNormalizedName() {
        return NormalizedName;
    }

    public void setNormalizedName(String normalizedName) {
        NormalizedName = normalizedName;
    }

    public String getMetaKeyword() {
        return MetaKeyword;
    }

    public void setMetaKeyword(String metaKeyword) {
        MetaKeyword = metaKeyword;
    }

    public String getMetaDescription() {
        return MetaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        MetaDescription = metaDescription;
    }

    public boolean getHomeFlag() {
        return HomeFlag;
    }

    public void setHomeFlag(boolean homeFlag) {
        HomeFlag = homeFlag;
    }

    public boolean getIsPublished() {
        return IsPublished;
    }

    public void setIsPublished(boolean isPublished) {
        IsPublished = isPublished;
    }
}
