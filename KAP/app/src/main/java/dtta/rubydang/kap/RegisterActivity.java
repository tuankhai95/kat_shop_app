package dtta.rubydang.kap;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.locks.Lock;

import class_kat.AccessToken;
import class_kat.GetAccessToken;
import class_kat.Post;
import class_kat.User;
import class_kat.productBrand;
import class_kat.readJSON;

public class RegisterActivity extends AppCompatActivity {
    Button btnRegister,btnBirthday;
    ImageButton imgBack;

    EditText edtFullName,edtAddress,edtPhone,edtPass,edtConfirmPass,edtNameLogin,edtEmail,edtBithday;
    AccessToken accessToken=new AccessToken();
    GetAccessToken re=new GetAccessToken();
    readJSON read=new readJSON();
    String s="";
    Calendar cal;
    Date date;
    String ID="",Error="";
    SimpleDateFormat sdf=new SimpleDateFormat("yyyy/MM/dd",Locale.getDefault());
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mapping();
        new TokenGet().execute();
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtFullName.getText().toString().equals("")||edtEmail.getText().toString().equals("")||edtPass.getText().toString().equals("")
                        ||edtAddress.getText().toString().equals("")||edtNameLogin.getText().toString().equals("")||edtPhone.getText().toString().equals("")
                        ||edtBithday.getText().toString().equals("")){
                    Toast.makeText(RegisterActivity.this,"Bạn vui lòng điền đầy dủ thông tin nhé !",Toast.LENGTH_LONG).show();
                }else {
                    if (edtPass.getText().toString().equals(edtConfirmPass.getText().toString())){
                        if (edtPass.getText().toString().length()>=6){
                            String json="{\"FullName\":\""+edtFullName.getText()+"\",\"BirthDay\":\""+edtBithday.getText()+"T00:00:00\",\"Email\":\""+edtEmail.getText()+"\"," +
                                    "\"Password\":\""+edtPass.getText()+"\",\"UserName\":\""+ edtNameLogin.getText()
                                    +"\",\"PhoneNumber\":\""+edtPhone.getText()+"\",\"Address\":\""+edtAddress.getText()+"\"}";

                            Log.d("Loijsonroi",json);
                            new Insert().execute(json);
                            //Log.d("Loijsonroi","Bằng nhau");
                            if (ID.toString().equals("yes")){
                                Toast.makeText(RegisterActivity.this,"Bạn đã đăng ký thành công",Toast.LENGTH_LONG).show();
                                Intent t=new Intent(RegisterActivity.this,LoginActivity.class);
                                startActivity(t);
                            }
                            if (Error.toString().equals("yes")){
                                Toast.makeText(RegisterActivity.this,Error.toString(),Toast.LENGTH_LONG).show();
                            }
                        }
                        else
                            Toast.makeText(RegisterActivity.this,"Mật khẩu phải lơn 6 trở lên kí tự!",Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(RegisterActivity.this,"Hai mật khẩu không khớp",Toast.LENGTH_LONG).show();
                    }
                }



            }
        });


    }

    private void mapping() {
        btnRegister=(Button)findViewById(R.id.buttonRegister);
        imgBack=(ImageButton)findViewById(R.id.imageButtonBackRegister);
        edtAddress=(EditText)findViewById(R.id.editTextAddress);
        edtFullName=(EditText)findViewById(R.id.editTextFullName);
        edtPhone=(EditText)findViewById(R.id.editTextPhone);
        edtConfirmPass=(EditText)findViewById(R.id.editTextConfirmPassWord);
        edtPass=(EditText)findViewById(R.id.editTextPassWord);
        edtNameLogin=(EditText)findViewById(R.id.editTextNameLogin);
        edtEmail=(EditText)findViewById(R.id.editTextEmail);
        btnBirthday=(Button)findViewById(R.id.buttonBirthday);
        edtBithday=(EditText)findViewById(R.id.editTextBirthday);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        cal=Calendar.getInstance();
        date=cal.getTime();
        btnBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog.OnDateSetListener dateSetListener=new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        cal.set(Calendar.YEAR,year);
                        cal.set(Calendar.MONTH,monthOfYear);
                        cal.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                        date=cal.getTime();
                        String s=sdf.format(date).replace("/","-");
                        edtBithday.setText(s);
                    }
                };
                DatePickerDialog timePickerDialog=new
                        DatePickerDialog(RegisterActivity.this,dateSetListener,cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH));
                timePickerDialog.show();
            }
        });

    }
    public class TokenGet extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            return re.getToken();
        }

        @Override
        protected void onPostExecute(String s) {

            try {
                JSONObject object=new JSONObject(s);
                accessToken.setAccess_token(object.getString("access_token"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("tokentest",accessToken.getAccess_token());
            super.onPostExecute(s);
        }
    }

    public class Insert extends AsyncTask<String ,Integer,String>{

        @Override
        protected String doInBackground(String... params) {
            return read.postData("account","register","registerViewModel",params[0],accessToken.getAccess_token());
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //Toast.makeText(RegisterActivity.this,s,Toast.LENGTH_LONG).show();
            try {
                JSONObject object=new JSONObject(s);
                if (object.getString("UserName").toString()!=null){
                    Intent t=new Intent(RegisterActivity.this,LoginActivity.class);
                    startActivity(t);
                }else {
                    return;
                }
                if (object.getString("Message").toString()!=null){
                    Toast.makeText(RegisterActivity.this,object.getString("Message").toString(),Toast.LENGTH_LONG).show();
                }else {
                    return;
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }
}
