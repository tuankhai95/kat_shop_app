package dtta.rubydang.kap;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import class_kat.AccessToken;
import class_kat.GetAccessToken;
import class_kat.Order;
import class_kat.OrderDetail;
import class_kat.Product;
import class_kat.SQLite;
import class_kat.readJSON;

public class PayActivity extends AppCompatActivity {

    EditText edtFullName,edtPhone,edtEmail,edtAddress,edtMessa,edtPayAddress;
    Button btn;
    ArrayList<OrderDetail> arr1;
    ImageButton imgbtn;
    readJSON read=new readJSON();
    GetAccessToken ac=new GetAccessToken();
    AccessToken accessToken=new AccessToken();
    long total;
    String order="";
    String payment="";
    int count=0;
    String name,phone,mail,address,message,payaddress;
    ArrayList<String>arrayList=new ArrayList<String>();

    Order o=new Order();
    final SQLite db=new SQLite(PayActivity.this,"cart.sqlite",null,1);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);
        mapping();
        Bundle bd=getIntent().getBundleExtra("OrderDetail");
        arr1=(ArrayList<OrderDetail>)bd.getSerializable("Detail");
        o.setArr(arr1);
        final SharedPreferences shre=getSharedPreferences(LoginActivity.prefname,MODE_PRIVATE);
        edtFullName.setText(shre.getString("FullName",""));
        edtEmail.setText(shre.getString("Email",""));
        edtAddress.setText(shre.getString("Address",""));
        edtPhone.setText(shre.getString("Phone",""));
        if (!Main2Activity.u.getID().equals("")){
            edtFullName.setText(LoginActivity.user.getFullName().toString());
            edtEmail.setText(LoginActivity.user.getEmail().toString());
            edtAddress.setText(LoginActivity.user.getAddress().toString());
            edtPhone.setText(LoginActivity.user.getPhoneNumber().toString());
            count=1;
        }

//        final SharedPreferences shre1=getSharedPreferences(Main2Activity.prefname,MODE_PRIVATE);
//
//            edtFullName.setText(shre1.getString("FullName",""));
//            edtAddress.setText(shre1.getString("Address",""));
//            edtPhone.setText(shre1.getString("Phone",""));
//            edtEmail.setText(shre1.getString("Email",""));


        new TokenGet().execute();
        for (int i=0;i<arr1.size();i++){
            long m;
            Product p=arr1.get(i).getProduct();
            if (p.getPromotionPrice()!=0){
                total+=p.getPromotionPrice()*arr1.get(i).getQuantity();
                m=p.getPromotionPrice();
            }else {
                total+=p.getPrice()*arr1.get(i).getQuantity();
                m=p.getPrice();
            }

            arrayList.add("{\"ProductID\":\""+arr1.get(i).getProduct().getID()+"\",\"Quantity\":\""+
                    arr1.get(i).getQuantity()+"\",\"Price\":\""+m+"\"}");
        }
        for (int j=0;j<arrayList.size();j++){
            if (j==arrayList.size()-1){
                order+=arrayList.get(j);
            }
            else {
                order+=arrayList.get(j)+",";
            }

        }
        Log.d("totaljson1",order);
        Log.d("totaljson1",total+"");

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name=edtFullName.getText().toString();
                phone=edtPhone.getText().toString();
                mail=edtEmail.getText().toString();
                address=edtAddress.getText().toString();
                message=edtMessa.getText().toString();
                payaddress=edtPayAddress.getText().toString();
                o.setCustomerName(name);
                o.setCustomerMobile(phone);
                o.setCustomerEmail(mail);
                o.setCustomerMessage(message);
                o.setCustomerBillingAddress(address);
                o.setCustomerShippingAddress(payaddress);
                if (count==1){
                    o.setCustomerId(LoginActivity.user.getID());
                    o.setCreatedBy(LoginActivity.user.getUserName());
                }else {
                    o.setCustomerId(shre.getString("Id",""));
                    o.setCreatedBy(shre.getString("username",""));
                }

                if(name.equals("")||phone.equals("")||mail.equals("")||address.equals("")||payaddress.equals("")||message.equals("")){
                    Toast.makeText(PayActivity.this,"Phải nhập đầy đủ thông tin nhé bạn!",Toast.LENGTH_LONG).show();
                }else {
                    Intent t=new Intent(PayActivity.this,PaymentsActivity.class);
                    Bundle bd=new Bundle();
                    bd.putSerializable("pay",o);
                    t.putExtra("Payment",bd);
                    startActivity(t);
                    String json="{\"CustomerName\":\""+edtFullName.getText()+"\",\"CustomerBuillingAddress\":\""+edtAddress.getText()+"\"," +
                            "\"CustomerEmail\":\""+edtEmail.getText()+"\",\"CustomerMobile\":\""+edtPhone.getText()+"\",\"CustomerMessage\":\""
                            +edtMessa.getText()+"\",\"PaymentMethod\":\""+payment+"\",\"CustomerShippingAddress\":\""+edtPayAddress.getText()+"\",\"CustomerId\":\""+
                            o.getCustomerId()+"\",\"SubTotal\":\"" +total+"\",\"OrderDetails\":[" +order+"]}";
                   new JSON().execute(json);
                    Log.d("totaljson1",json);
                }
//                edtAddress.setText("");
//                edtFullName.setText("");
//                edtEmail.setText("");
//                edtMessa.setText("");
//                edtPayAddress.setText("");
//                edtPhone.setText("");
                db.QueryData("delete from Product");
                BuyActivity.arr.clear();
                BuyActivity.total=0;
                BuyActivity.txtTotal.setText("0 VND");
                AlertDialog.Builder b=new AlertDialog.Builder(PayActivity.this);
                b.setTitle("Bạn có muốn qua về trang chủ không!");
                b.setMessage("Chúng tôi đã nhận được đơn hàng của bạn chúng tôi sẽ giao hàng trong thời gian sớm nhất cho bạn!");
                b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent t=new Intent(PayActivity.this,Main2Activity.class);
                        startActivity(t);
                    }
                });
                b.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                b.create().show();

            }
        });

    }
    class JSON extends AsyncTask<String,Integer,String>{

        @Override
        protected String doInBackground(String... params) {
            return read.postData("order","create","orderViewModel",params[0],accessToken.getAccess_token());
        }

        @Override
        protected void onPostExecute(String s) {
            Log.d("ThanhCong",s);

            super.onPostExecute(s);
        }
    }

    private void mapping() {
        edtFullName=(EditText)findViewById(R.id.editTextPayFullName);
        edtPhone=(EditText)findViewById(R.id.editTextPayPhone);
        edtEmail=(EditText)findViewById(R.id.editTextPayEmail);
        edtAddress=(EditText)findViewById(R.id.editTextAddress);
        btn=(Button)findViewById(R.id.buttonDacMua);
        edtMessa=(EditText)findViewById(R.id.editTextMessage);
        edtPayAddress=(EditText)findViewById(R.id.editTextPayAddress);
        imgbtn=(ImageButton)findViewById(R.id.imageButtonBackPay);
        imgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
    private class TokenGet extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            return ac.getToken();
        }

        @Override
        protected void onPostExecute(String s) {


            try {
                JSONObject object = new JSONObject(s);
                accessToken.setAccess_token(object.getString("access_token"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("tokentest", accessToken.getAccess_token());
            super.onPostExecute(s);
        }
    }
}
