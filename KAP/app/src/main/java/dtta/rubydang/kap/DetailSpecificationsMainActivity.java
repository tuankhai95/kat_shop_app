package dtta.rubydang.kap;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import class_kat.Product;
import class_kat.SystemPath;
import class_kat.readJSON;

public class DetailSpecificationsMainActivity extends AppCompatActivity {

    Product p;
    readJSON read=new readJSON();
    LinearLayout layout;
    android.os.Handler handler;
    TextView b;
    AtomicBoolean atomic=new AtomicBoolean(false);
    ImageButton imgBtn;
    ArrayList<String>arr=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_specifications_main);
        Bundle bd=getIntent().getBundleExtra("ProductBundel");
        p=(Product)bd.getSerializable("Product");
        //Toast.makeText(DetailSpecificationsMainActivity.this,p.getName(),Toast.LENGTH_LONG).show();
        layout=(LinearLayout)findViewById(R.id.lin);
        imgBtn=(ImageButton)findViewById(R.id.imageButtonBackSpecification) ;

        handler=new Handler(){
            public void handleMessage(Message message){
                super.handleMessage(message);
                String nhanbtn=message.obj.toString();
                b=new TextView(DetailSpecificationsMainActivity.this);
                b.setText(nhanbtn);
                if (nhanbtn.equals("Màn hình".toUpperCase())||nhanbtn.equals("Camera".toUpperCase())||nhanbtn.equals("Hệ điều hành".toUpperCase())
                        ||nhanbtn.equals("Bộ nhớ & Lưu trữ".toUpperCase())
                        ||nhanbtn.equals("Kết nối".toUpperCase())||nhanbtn.equals("Thiết kế & Trọng lượng".toUpperCase())
                        ||nhanbtn.equals("Thông tin pin".toUpperCase())||
                        nhanbtn.equals("Giải trí & Ứng dụng".toUpperCase())||nhanbtn.equals("Bộ xử lý".toUpperCase())
                        ||nhanbtn.equals("Bo mạch".toUpperCase())
                        ||nhanbtn.equals("Đĩa cứng".toUpperCase())||
                        nhanbtn.equals("Card Reader".toUpperCase())||nhanbtn.equals("Giao tiếp mạng".toUpperCase())||nhanbtn.equals("Đồ họa".toUpperCase())
                        ||nhanbtn.equals("Đĩa quang".toUpperCase())){
                    b.setBackgroundColor(Color.GRAY);
                    b.setPadding(0,10,0,10);
                    b.setTextSize(18);
                }
                b.setTextColor(Color.BLACK);
                b.setTextSize(16);
                b.setPadding(0,6,0,6);
                LinearLayout.LayoutParams params=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                b.setLayoutParams(params);
                layout.addView(b);
            }
        };
        new ReadJSONFeedTask().execute(SystemPath.url+"api/product/getbyid/"+p.getID());
        imgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private class ReadJSONFeedTask extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return read.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray object2=jsonObject.getJSONArray("AttributeValues");
                int z=0,z1,z2,z3,z4,z5,z6,z7,z8,z13,z9,z10,z11,z12,z14;
                z1=z2=z3=z4=z5=z6=z7=z8=z13=z9=z10=z11=z12=z14=0;
                for (int i=0;i<object2.length();i++) {
                    final JSONObject json=object2.getJSONObject(i);
                    final JSONObject mang1=json.getJSONObject("Attribute");
                    int k=Integer.parseInt(mang1.getString("ProductAttributeGroupId"));
                   // doStart("Màn hình");

                    switch (k){
                        case 12:{//Màn hình
                            if (z==0){
                                doStart("Màn hình".toUpperCase());
                                z++;
                            }
                            doStart(mang1.getString("Name") +": "+json.getString("Value"));
                            //doStart("\n");
                            break;
                        }
                        case 13:{//Camera
                            if (z1==0){
                                doStart("Camera".toUpperCase());
                                z1++;
                            }
                            doStart(mang1.getString("Name") +": "+json.getString("Value"));
                            //doStart("\n");
                            break;
                        }
                        case 14:{//Hệ điều hành
                            if (z2==0){
                                doStart("Hệ điều hành".toUpperCase());
                                z2++;
                            }
                            doStart(mang1.getString("Name") +": "+json.getString("Value"));
                            break;
                        }
                        case 15:{//Bộ nhớ & Lưu trữ
                            if (z3==0){
                                doStart("Bộ nhớ & Lưu trữ".toUpperCase());
                                z3++;
                            }
                            doStart(mang1.getString("Name") +": "+json.getString("Value"));
                            break;
                        }
                        case 16:{//Kết nối
                            if (z4==0){
                                doStart("Kết nối".toUpperCase());
                                z4++;
                            }
                            doStart(mang1.getString("Name") +": "+json.getString("Value"));
                            break;
                        }
                        case 17:{//Thiết kế & Trọng lượng
                            if (z5==0){
                                doStart("Thiết kế & Trọng lượng".toUpperCase());
                                z5++;
                            }
                            doStart(mang1.getString("Name") +": "+json.getString("Value"));
                            break;
                        }
                        case 18:{//Thông tin pin
                            if (z6==0){
                                doStart("Thông tin pin".toUpperCase());
                                z6++;
                            }
                            doStart(mang1.getString("Name") +": "+json.getString("Value"));
                            break;
                        }
                        case 19:{//Giải trí & Ứng dụng
                            if (z7==0){
                                doStart("Giải trí & Ứng dụng".toUpperCase());
                                z7++;
                            }
                            doStart(mang1.getString("Name") +": "+json.getString("Value"));
                            break;
                        }
                        case 20:{//Bộ xử lý
                            if (z8==0){
                                doStart("Bộ xử lý".toUpperCase());
                                z8++;
                            }
                            doStart(mang1.getString("Name") +": "+json.getString("Value"));
                            break;
                        }
                        case 21:{//Bo mạch
                            if (z9==0){
                                doStart("Bo mạch".toUpperCase());
                                z9++;
                            }
                            doStart(mang1.getString("Name") +": "+json.getString("Value"));
                            break;
                        }
                        case 22:{//Đĩa cứng
                            if (z10==0){
                                doStart("Đĩa cứng".toUpperCase());
                                z10++;
                            }
                            doStart(mang1.getString("Name") +": "+json.getString("Value"));
                            break;
                        }
                        case 23:{//Đồ họa
                            if (z11==0){
                                doStart("Đồ họa".toUpperCase());
                                z11++;
                            }
                            doStart(mang1.getString("Name") +": "+json.getString("Value"));
                            break;
                        }
                        case 24:{//Đĩa quang
                            if (z12==0){
                                doStart("Đĩa quang".toUpperCase());
                                z12++;
                            }
                            doStart(mang1.getString("Name") +": "+json.getString("Value"));
                            break;
                        }
                        case 25:{//Giao tiếp mạng
                            if (z13==0){
                                doStart("Giao tiếp mạng".toUpperCase());
                                z13++;
                            }
                            doStart(mang1.getString("Name") +": "+json.getString("Value"));
                            break;
                        }
                        case 26:{//Card Reader
                            if (z14==0){
                                doStart("Card Reader".toUpperCase());
                                z14++;
                            }
                            doStart(mang1.getString("Name") +": "+json.getString("Value"));
                            break;
                        }

                    }

                   // Toast.makeText(DetailSpecificationsMainActivity.this,""+z,Toast.LENGTH_LONG).show();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void doStart( final String s){
        Thread th=new Thread(new Runnable() {
            @Override
            public void run() {
                    Message msg=handler.obtainMessage();
                    msg.obj=s;
                    handler.sendMessage(msg);
            }
        });
        atomic.set(true);
        th.start();
    }
}
