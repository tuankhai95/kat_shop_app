package dtta.rubydang.kap;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;

import class_kat.Product;
import class_kat.SystemPath;
import class_kat.readJSON;

public class DetailProductMainActivity extends AppCompatActivity {

    readJSON read=new readJSON();
    ImageButton imgbtnBackDetail;
    TextView txtCategoyProduct,txtProductBrand,txtMoreImage,txtDetailProduct,txtSpecitication,txtWarranty,txtPrice,txtQuantity;
    TextView txtNameProduct,txtGioiThieu;
    Button btnBuy;
    ImageView imgImageProduct;
    ArrayList<String> arr=new ArrayList<String>();
    Product p;
    WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product_main);
        Mapping();
        dataProcessing();
        Bundle bd=getIntent().getBundleExtra("product");
        p=(Product)bd.getSerializable("productBundel");

        new ReadJSONFeedTask().execute(SystemPath.url+"api/product/getbyid/"+p.getID());
        btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (p.getQuantity()!=0){
                    Intent t=new Intent(DetailProductMainActivity.this,BuyActivity.class);
                    Bundle bd=new Bundle();
                    bd.putSerializable("productbuy",p);
                    t.putExtra("product",bd);
                    startActivity(t);
                }else {
                    Toast.makeText(DetailProductMainActivity.this,"Sản phẩm đã hết hàng",Toast.LENGTH_LONG).show();
                }

            }
        });
    }
    public void Mapping(){
        imgbtnBackDetail=(ImageButton)findViewById(R.id.imageButtonBackDetail);
        txtCategoyProduct=(TextView)findViewById(R.id.textViewCategoryProduct);
        txtProductBrand=(TextView)findViewById(R.id.textViewProductBrand);
        txtMoreImage=(TextView)findViewById(R.id.textViewMoreImage);
        txtDetailProduct=(TextView)findViewById(R.id.textViewDetailProduct);
        txtSpecitication=(TextView)findViewById(R.id.textViewSpecitification);
        txtWarranty=(TextView)findViewById(R.id.textViewWarranty);
        txtPrice=(TextView)findViewById(R.id.textViewPrice);
        txtQuantity=(TextView)findViewById(R.id.textViewQuantity);
        btnBuy=(Button)findViewById(R.id.buttondatmua);
        imgImageProduct=(ImageView)findViewById(R.id.imageViewProduct);
        txtNameProduct=(TextView)findViewById(R.id.textViewNameProduct);
       // txtGioiThieu=(TextView)findViewById(R.id.textViewGiothieu);
        webView = (WebView)findViewById(R.id.webView);

        WebSettings webSetting = webView.getSettings();
        webSetting.setBuiltInZoomControls(false);
        webSetting.setJavaScriptEnabled(true);
        //webSetting.setBlockNetworkLoads(img);
        webView.setWebViewClient(new WebViewClient());

    }
    public void dataProcessing(){
        imgbtnBackDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        txtMoreImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent t=new Intent(DetailProductMainActivity.this,MoreImageActivity.class);
                Bundle bd=new Bundle();
                bd.putSerializable("Image",arr);
                t.putExtra("MoreImage",bd);
                startActivity(t);
            }
        });
        txtSpecitication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent t1=new Intent(DetailProductMainActivity.this,DetailSpecificationsMainActivity.class);
                Bundle bd1=new Bundle();
                bd1.putSerializable("Product",p);
                t1.putExtra("ProductBundel",bd1);
                startActivity(t1);
            }
        });

    }
    private class ReadJSONFeedTask extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return read.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                txtNameProduct.setText(jsonObject.getString("Name"));
                Picasso.with(DetailProductMainActivity.this).load(jsonObject.getString("Image")).into(imgImageProduct);
                txtPrice.setText(jsonObject.getInt("Price")+" VND");
                if (jsonObject.getInt("Quantity")==0){
                    txtQuantity.setText("Hết hàng");
                }else {
                    txtQuantity.setText("Còn hàng");
                }
                txtWarranty.setText(jsonObject.getInt("Warranty")+" tháng");
                txtDetailProduct.setText(jsonObject.getString("Description").toString());
                JSONObject object=jsonObject.getJSONObject("ProductBrand");
                Log.d("object.getString",object.getString("Name"));
                txtProductBrand.setText(object.getString("Name")+"/ ");
                JSONObject object1=jsonObject.getJSONObject("ProductCategory");
                txtCategoyProduct.setText(object1.getString("Name")+"/ ");
                String moreImage=jsonObject.getString("MoreImages");
                webView.loadUrl("file:///android_asset/first1.html");
                //Toast.makeText(DetailProductMainActivity.this,jsonObject.getString("Content")+"",Toast.LENGTH_LONG).show();
                String data = jsonObject.getString("Content").toString();
                if (data.equals("")){
                    webView.loadDataWithBaseURL(null,"<b>Chưa có giới thiệu về sản phẩm này</b>" , "text/html", "utf-8",null);
                }else {
                    webView.loadDataWithBaseURL(null,data , "text/html", "utf-8",null);
                    webView.loadDataWithBaseURL(null, "<style>img{display: inline;height: auto;max-width: 100%;} a{text-decoration: none;color:black;pointer-events: none;}</style>" + data, "text/html", "UTF-8", null);
                }
                moreImage=moreImage.replace("[","");
                moreImage=moreImage.replace("]",";");
                moreImage=moreImage.replace("\",\"",";");
                moreImage=moreImage.replace("\"","");
                moreImage=moreImage.trim();
                moreImage.toCharArray();
                String p="http://katshop.tuankhai.xyz",q="";
                ArrayList<Integer>stt=new ArrayList<Integer>();
                stt.add(0);
                int k = 0;
                for (int i=0;i<moreImage.length();i++){
                        switch (moreImage.charAt(i)){
                            case ';':{
                                stt.add(i);
                                break;
                            }
                        }
                }
                for (int j=0;j<stt.size();j++){
                    String kq=moreImage.substring(stt.get(j),stt.get(j+1));
                    //kq=kq.replace(";","");
                    arr.add("http://katshop.tuankhai.xyz"+kq.trim().replace(";",""));
                    //Toast.makeText(DetailProductMainActivity.this,"http://katshop.tuankhai.xyz"+kq.trim().replace(";",""),Toast.LENGTH_LONG).show();
                }


//
//                String s="";
//                if (object1.getString("Name").equals("Điện thoại")){
//                    JSONArray object2=jsonObject.getJSONArray("AttributeValues");
//                    for (int i=0;i<object2.length();i++){
//                        JSONObject mang=object2.getJSONObject(i);
//                        JSONObject mang1=mang.getJSONObject("Attribute");
//
//                        //txtDetailProduct.setText(mang1.getString("Name")+"----"+mang.getString("Value"));
//                        if (mang1.getString("Name").equals("Màn hình rộng")){
//                            //txtDetailProduct.setText("");
//                            s+=mang1.getString("Name")+" "+mang.getString("Value")+" / ";
//                        }
//                        if (mang1.getString("Name").equals("Camera sau")){
//                            s+=mang1.getString("Name")+" "+mang.getString("Value")+" / ";
//                        }
//                        if (mang1.getString("Name").equals("RAM")){
//                            s+=mang1.getString("Name")+" "+mang.getString("Value")+" / ";
//                        }
//                        if (mang1.getString("Name").equals("Dung lượng pin")){
//                            s+=mang1.getString("Name")+" "+mang.getString("Value")+"";
//                        }
//                        if (mang1.getString("Name").equals("Số khe sim")){
//                            s+=mang1.getString("Name")+" "+mang.getString("Value")+" / ";
//                        }
//                    }
//                    txtDetailProduct.setText(s);
//                    //Toast.makeText(DetailProductMainActivity.this,s,Toast.LENGTH_LONG).show();
//                }else if (object1.getString("Name").equals("Laptop")){
//                    JSONArray object2=jsonObject.getJSONArray("AttributeValues");
//                    for (int i=0;i<object2.length();i++){
//                        JSONObject mang=object2.getJSONObject(i);
//                        JSONObject mang1=mang.getJSONObject("Attribute");
//
//                        //txtDetailProduct.setText(mang1.getString("Name")+"----"+mang.getString("Value"));
//                        if (mang1.getString("Name").equals("Màn hình")){
//                            //txtDetailProduct.setText("");
//                            s+=mang1.getString("Name")+" "+mang.getString("Value")+" / ";
//                        }
//                        if (mang1.getString("Name").equals("Bộ xử lý")){
//                            s+=mang1.getString("Name")+" "+mang.getString("Value")+" / ";
//                        }
//                        if (mang1.getString("Name").equals("RAM")){
//                            s+=mang1.getString("Name")+" "+mang.getString("Value")+" / ";
//                        }
//                        if (mang1.getString("Name").equals("Chip đồ họa (GPU)")){
//                            s+=mang1.getString("Name")+" "+mang.getString("Value")+" / ";
//                        }
//                        if (mang1.getString("Name").equals("Công nghệ CPU")){
//                            s+=mang1.getString("Name")+" "+mang.getString("Value")+"  ";
//                        }
//                    }
//                    txtDetailProduct.setText(s);
//                    //Toast.makeText(DetailProductMainActivity.this,s,Toast.LENGTH_LONG).show();
//                }
//                else if (object1.getString("Name").equals("Máy tính bảng")){
//                    JSONArray object2=jsonObject.getJSONArray("AttributeValues");
//                    for (int i=0;i<object2.length();i++){
//                        JSONObject mang=object2.getJSONObject(i);
//                        JSONObject mang1=mang.getJSONObject("Attribute");
//
//                        //txtDetailProduct.setText(mang1.getString("Name")+"----"+mang.getString("Value"));
//                        if (mang1.getString("Name").equals("Màn hình rộng")){
//                            //txtDetailProduct.setText("");
//                            s+=mang1.getString("Name")+" "+mang.getString("Value")+" / ";
//                        }
//                        if (mang1.getString("Name").equals("Loại Sim")){
//                            s+=mang1.getString("Name")+" "+mang.getString("Value")+" / ";
//                        }
//                        if (mang1.getString("Name").equals("RAM")){
//                            s+=mang1.getString("Name")+" "+mang.getString("Value")+" / ";
//                        }
//                        if (mang1.getString("Name").equals("Dung lượng pin")){
//                            s+=mang1.getString("Name")+" "+mang.getString("Value")+"";
//                        }
//                    }
//                    txtDetailProduct.setText(s);
//                    //Toast.makeText(DetailProductMainActivity.this,s,Toast.LENGTH_LONG).show();
//                }
//                else{
//                    txtDetailProduct.setText("");
//                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
