package dtta.rubydang.kap;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CustomMoreImageActivity extends ArrayAdapter<String > {
    Activity context;
    int layoutID;
    ArrayList<String>arr;


    public CustomMoreImageActivity(Activity context, int layoutID,ArrayList<String>aa) {
        super(context, layoutID,aa);
        this.context=context;
        this.layoutID=layoutID;
        arr=aa;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView=context.getLayoutInflater().inflate(layoutID,null);
        ImageView img=(ImageView)convertView.findViewById(R.id.imageViewCustomMoreImage);
        String s=arr.get(position);
        Picasso.with(context).load(s).into(img);
        return convertView;
    }
}
