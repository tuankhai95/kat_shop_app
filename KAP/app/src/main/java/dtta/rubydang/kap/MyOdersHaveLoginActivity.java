package dtta.rubydang.kap;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import class_kat.Order;
import class_kat.OrderDetail;
import class_kat.Product;
import class_kat.SystemPath;
import class_kat.readJSON;

public class MyOdersHaveLoginActivity extends AppCompatActivity {

    TextView txtFullname,txtDate,txtTotalMoney,txtStatus,txtStatusOser;
    ImageButton btn;
    ListView lv;
    ArrayList<OrderDetail> arr=new ArrayList<OrderDetail>();
    CustomListProductOrderActivity adapter;
    readJSON read=new readJSON();
    Order o;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_oders_have_login);
        Bundle bd=getIntent().getBundleExtra("Order");
        o=(Order)bd.getSerializable("MyOrder");
        mapping();
        new readJSONOrderNoLogin().execute(SystemPath.url+"api/order/getbyemailandid?email="+o.getCustomerEmail()+"&id="+o.getID());


    }
    private void mapping() {
        txtFullname=(TextView)findViewById(R.id.textViewFullname1);
        txtDate=(TextView)findViewById(R.id.textViewDate1);
        txtStatus=(TextView)findViewById(R.id.textViewStutas1);
        txtStatusOser=(TextView)findViewById(R.id.textViewStatusOrder1);
        txtTotalMoney=(TextView)findViewById(R.id.textViewTotalMoney1);
        btn=(ImageButton)findViewById(R.id.imageButtonBackOrderNoLogin1);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        lv=(ListView)findViewById(R.id.listView1);
        adapter=new CustomListProductOrderActivity(MyOdersHaveLoginActivity.this,R.layout.activity_custom_list_product_order,arr);
        lv.setAdapter(adapter);
    }
    class readJSONOrderNoLogin extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {
            return read.readJSONFeed(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            try {//Toast.makeText(LoginActivity.this,s,Toast.LENGTH_LONG).show();
                JSONObject jsonObject = new JSONObject(s);
                txtFullname.setText("Họ và tên: "+jsonObject.getString("CustomerName"));
                String date=jsonObject.getString("CreatedDate");
                date=date.replace(date.substring(10),"");
                txtDate.setText("Ngày đặt hàng: "+date);
                txtStatus.setText("Trạng thái đơn hàng: "+NoLoginActivity.status(jsonObject.getInt("Status")));
                txtStatusOser.setText("Trạng thái thanh toán: "+NoLoginActivity.PaymentStatus(jsonObject.getInt("PaymentStatus")));
                txtTotalMoney.setText("Tổng tiền: "+jsonObject.getInt("SubTotal")+"VND");
                Order o=new Order();
                o.setID(jsonObject.getInt("ID"));
                o.setCustomerEmail(jsonObject.getString("CustomerEmail"));
                o.setCustomerName(jsonObject.getString("CustomerName"));
                JSONArray jsonArray=jsonObject.getJSONArray("OrderDetails");
                for (int i=0;i<jsonArray.length();i++){
                    JSONObject object=jsonArray.getJSONObject(i);
                    OrderDetail od=new OrderDetail();
                    od.setQuantity(object.getInt("Quantity"));
                    String k=""+object.getInt("Price");
                    od.setPrice(Long.parseLong(k));
                    JSONObject object1=object.getJSONObject("Product");
                    Product p=new Product();
                    p.setName(object1.getString("Name"));
                    p.setImageProduct(object1.getString("Image"));
                    od.setProduct(p);
                    arr.add(od);
                    adapter.notifyDataSetChanged();
                }//Toast.makeText(NoLoginActivity.this,arr.get(1).getProduct().getName()+"",Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }
}
