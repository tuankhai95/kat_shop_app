package dtta.rubydang.kap;


import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpAuthentication;
import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;

import class_kat.AccessToken;
import class_kat.Authentication;
import class_kat.GetAccessToken;
import class_kat.Infor;
import class_kat.Post;
import class_kat.PostCategory;
import class_kat.Product;
import class_kat.SystemPath;
import class_kat.User;
import class_kat.productBrand;
import class_kat.productCategory;
import class_kat.readJSON;

import static android.content.ContentValues.TAG;
import static java.lang.System.in;
import static java.lang.System.out;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {

    //TabHost tb;
    GridView gv;
    Product product=null;
    ArrayList<PostCategory>arr1=new ArrayList<PostCategory>();
    readJSON read1=new readJSON();
    ArrayList<productBrand>arrproductBrand=new ArrayList<productBrand>();
    ArrayList<Product>pro=new ArrayList<Product>();
    ArrayList<Product>pro1=new ArrayList<Product>();
    ArrayList<Product>productCat=new ArrayList<Product>();
    ArrayList<Product>productSearch=new ArrayList<Product>();
    Spinner spBrand,spRankedByPrice;
    CustomHomeActivity adapter=null;
    ArrayList<Product>arrayProduct=new ArrayList<Product>();
    ArrayList<String>arrayListSpBrand=new ArrayList<String>();
    ArrayAdapter<String>adapterSpBrand;
    ArrayList<String>arrayListSpRankedByPrice=new ArrayList<String>();
    ArrayAdapter<String>adapterSpRankedByPrice;
    ArrayList<productCategory>categoryArrayList=new ArrayList<productCategory>();
    ArrayList<Post>arr=new ArrayList<Post>();
    readJSON read2=new readJSON();
    readJSON read=new readJSON();
    readJSON read3=new readJSON();
    int a=0,b=0,c=0,k=0;
    static String s="";
    static User u;
    private boolean isStarted = false;
    public static String prefname="my_data";
    private SearchView searchView;
    public  static SharedPreferences appPrefs;
    NavigationView mDrawerList;
    Menu menu,topChannelMenu,topChannelMenuPost;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        appPrefs = getSharedPreferences(prefname, MODE_PRIVATE);
        SharedPreferences share=getSharedPreferences(LoginActivity.prefname,MODE_PRIVATE);
        if (!share.getString("FullName", "").equals("")){
                   s=share.getString("FullName", "");
                }
        if(getIntent().getBundleExtra("Login")==null){
        }else{
            Bundle bd=getIntent().getBundleExtra("Login");
            u=(User)bd.getSerializable("User");
            if (!u.getEmail().equals("")){
                s=u.getFullName();
            }

        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        new readJSONPost().execute(SystemPath.url+"api/post/getallparents");
        new ReadJSONBrand().execute(SystemPath.url+"api/productbrand/getallparents");
        new ReadJSONFeedTask().execute(SystemPath.url+"api/product/getallparents");
        //new TokenGet().execute();
        gvHome();
        dataSpinner();
        //change nav
        mDrawerList = (NavigationView) findViewById(R.id.nav_view);
        menu = mDrawerList.getMenu();
        topChannelMenu = menu.addSubMenu("Loại sản phẩm");
        new ReadJSONCategory().execute(SystemPath.url+"api/productcategory/getallparents");
        topChannelMenuPost=menu.addSubMenu("Tin tức");
        new readJSONPostCategory().execute(SystemPath.url+"api/postcategory/getallparents");
    }
    @Override
    public void onPause() {
        super.onPause();
        Log.v(TAG, "- ON PAUSE -");
    }

    @Override
    public void onStop() {
        super.onStop();
        SharedPreferences share=getSharedPreferences(LoginActivity.prefname,MODE_PRIVATE);
        SharedPreferences.Editor editor=share.edit();
        if (LoginActivity.temp==1){
            editor.putString("username","");
            editor.putString("password","");
            editor.putString("Email","");
            editor.putString("Id","");
            editor.putString("FullName","");
            editor.putString("Address","");
            editor.putString("Phone","");
            editor.commit();

        }

        Log.v("TAGTOP", "-- ON STOP --"+share.getString("username",""));

    }
    private void gvHome() {
        gv=(GridView)findViewById(R.id.gridViewHome);
        adapter=new CustomHomeActivity(Main2Activity.this,R.layout.activity_custom_home,arrayProduct);
        gv.setAdapter(adapter);
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(Main2Activity.this,position+"",Toast.LENGTH_LONG).show();
                Intent t=new Intent(Main2Activity.this,DetailProductMainActivity.class);
                Bundle bd=new Bundle();
                bd.putSerializable("productBundel",arrayProduct.get(position));
                t.putExtra("product",bd);
                startActivity(t);
            }
        });
        spBrand=(Spinner)findViewById(R.id.spinnerBrand);
        adapterSpBrand=new ArrayAdapter<String>(Main2Activity.this,android.R.layout.simple_spinner_item,arrayListSpBrand);
        adapterSpBrand.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spBrand.setAdapter(adapterSpBrand);

        spRankedByPrice=(Spinner)findViewById(R.id.spinnerRankedByPrice);
        adapterSpRankedByPrice=new ArrayAdapter<String>(Main2Activity.this,android.R.layout.simple_spinner_item,arrayListSpRankedByPrice);
        adapterSpRankedByPrice.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spRankedByPrice.setAdapter(adapterSpRankedByPrice);


    }

    public void dataSpinner(){
        arrayListSpRankedByPrice.add("Xem theo");
        arrayListSpRankedByPrice.add("Giá cao");
        arrayListSpRankedByPrice.add("Giá thấp");
        adapterSpRankedByPrice.notifyDataSetChanged();

        arrayListSpBrand.add("Tất cả hãng");
        adapterSpBrand.notifyDataSetChanged();
        spBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                a=position;
                spinnerBrand(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//        spPrice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                //Toast.makeText(Main2Activity.this,a+" "+c,Toast.LENGTH_LONG).show();
//                b=position;
//                spinnerPrice(position);
////                if (c!=0){
////                    spinnerRankByPrice(c);
////                }
////                if (a!=0){
////                    spinnerBrand(a);
////                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
        spRankedByPrice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spinnerRankByPrice(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main2, menu);
        if (!s.equals("")){
            menu.add(0,1,0,"Chào "+s);
            menu.add(0,4,0,"Đăng xuất");
        }else {
            menu.add(0,1,0,"Đăng nhập");
        }

        MenuItem search=menu.findItem(R.id.search);
        searchView =(SearchView)search.getActionView();
        searchView.setOnQueryTextListener(this);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id){
            case R.id.dangki:{
                //Toast.makeText(Main2Activity.this, "Đăng Kí", Toast.LENGTH_SHORT).show();
                Intent t5=new Intent(Main2Activity.this,RegisterActivity.class);
                startActivity(t5);
                break;
            }
            case 1:{
               SharedPreferences share=getSharedPreferences(prefname,MODE_PRIVATE);
                SharedPreferences.Editor editor=share.edit();
                SharedPreferences shre=getSharedPreferences(LoginActivity.prefname,MODE_APPEND);

                editor.putString("FullName",s);
                editor.putString("Id",shre.getString("ID",""));
                editor.putString("username",shre.getString("UserName",""));
                editor.putString("Email",shre.getString("Email",""));
                editor.putString("Address",shre.getString("Address",""));
                editor.putString("Phone",shre.getString("Phone",""));
                editor.commit();
                Intent t4=new Intent(Main2Activity.this,LoginActivity.class);
                startActivity(t4);
                break;
            }
            case R.id.donhang:{
                SharedPreferences shre=getSharedPreferences(LoginActivity.prefname,MODE_APPEND);


                if (!PaymentsActivity.temp.equals("")){
                    //Toast.makeText(Main2Activity.this,PaymentsActivity.temp+"",Toast.LENGTH_LONG).show();
                    User user=new User();
                    user.setID(PaymentsActivity.temp);
                    Intent t4=new Intent(Main2Activity.this,MyOrdersActivity.class);
                    Bundle bd=new Bundle();
                    bd.putSerializable("User",user);
                    t4.putExtra("Login",bd);
                    startActivity(t4);
                }
                else if (!shre.getString("Id", "").equals("")){
                    User user=new User();
                    user.setID(shre.getString("Id", ""));
                    Intent t4=new Intent(Main2Activity.this,MyOrdersActivity.class);
                    Bundle bd=new Bundle();
                    bd.putSerializable("User",user);
                    t4.putExtra("Login",bd);
                    startActivity(t4);
                }else {
                    Intent t3=new Intent(Main2Activity.this,OrdersActivity.class);
                    startActivity(t3);
                }

                break;

            }case 4:{
                Toast.makeText(Main2Activity.this, "Bạn vừa đăng xuất", Toast.LENGTH_SHORT).show();
                isStarted=true;
                SharedPreferences share=getSharedPreferences(LoginActivity.prefname,MODE_PRIVATE);
                SharedPreferences.Editor editor=share.edit();
                SharedPreferences.Editor editor1=appPrefs.edit();
                editor1.putString("user","");
                if (false){
                    editor.clear();
                }else {
                    editor.putString("username","");
                    editor.putString("password","");
                    editor.putString("Email","");
                    editor.putString("Id","");
                    editor.putString("FullName","");
                    editor.putString("Address","");
                    editor.putString("Phone","");
                    editor.putBoolean("remember",false);
                }
                editor.commit();
                break;
            }
            case R.id.gopy:{
                Intent t2=new Intent(Main2Activity.this,FeedBackActivity.class);
                startActivity(t2);
                break;
            }
            case R.id.cart:{
                Intent t3=new Intent(Main2Activity.this,BuyActivity.class);
                Bundle bd=new Bundle();
                Product p=new Product();
                p.setImageProduct("");
                bd.putSerializable("productbuy",p);
                t3.putExtra("product",bd);
                startActivity(t3);
                break;
            }

            default:{

                break;
            }


        }


        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if(isStarted) {
            menu.removeItem(4);
            menu.removeItem(1);
            menu.add(0,1,0,"Đăng nhập");
        } else {
            //Toast.makeText(Main2Activity.this, "Khong", Toast.LENGTH_SHORT).show();
        }
        return true;
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        b=1;
        //new ReadJSONFeedTask().execute("http://katshop.tuankhai.xyz/api/product/getallparents");
        for (int i=0;i<arrayProduct.size();i++) {
            delete(i);
        }


        if (productCat.size()==0)
        for (int i=0;i<pro.size();i++){
            productCat.add(pro.get(i));
        }
        if (id == R.id.trangchu) {
            for (int i=0;i<arrayProduct.size();i++) {
                delete(i);
            }
            for (int i=0;i<pro.size();i++){
                pro.remove(i);
                pro.clear();
                adapter.notifyDataSetChanged();
                new ReadJSONFeedTask().execute(SystemPath.url+"api/product/getallparents");
            }

        }
        for (int i=0;i<arrayProduct.size();i++) {
            delete(i);
        }
        for (int q=0;q<categoryArrayList.size();q++){
            if (item.getTitle().equals(categoryArrayList.get(q).getName())){
                int k=0,m=0;
                for (int i=0;i<productCat.size();i++){
                    //Toast.makeText(Main2Activity.this,""+productCat.get(i).getCategory().getName(),Toast.LENGTH_LONG).show();
                   if (categoryArrayList.get(q).getName().equals(productCat.get(i).getCategory().getName())){
                       arrayProduct.add(productCat.get(i));
                       adapter.notifyDataSetChanged();
                       k++;
                   }

                }
                if (k==0){
                    AlertDialog.Builder b=new AlertDialog.Builder(Main2Activity.this);
                    b.setMessage("Chưa có sản phẩm về "+categoryArrayList.get(q).getName());
                    b.setNegativeButton("Thoát", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();

                        }
                    });
                    b.create().show();
                }
            }

        }


        for (int j=0;j<arr1.size();j++){

            if (item.getTitle().equals(arr1.get(j).getName().toString())) {
                int k=0,m=0;
                for (int i=0;i<arr.size();i++){
                    if (arr.get(i).getPostCategory().getID()==arr1.get(j).getID()){
                        k++;
                        m=arr.get(i).getPostCategory().getID();
                    }
                }
                if (k==0){
                    AlertDialog.Builder b=new AlertDialog.Builder(Main2Activity.this);
                    b.setMessage("Chưa có tin tức về "+arr1.get(j).getName());
                    b.setNegativeButton("Thoát", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();

                        }
                    });
                    b.create().show();
                }else {
                    Intent t=new Intent(Main2Activity.this,PostActivity.class);
                    t.putExtra("tintuc",m);
                    startActivity(t);
                }

            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        k=1;
        //if()
        new ReadJSONSearch().execute(SystemPath.url+"api/product/getlistproductbyname?productName="+query);

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }


    private class ReadJSONFeedTask extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return read.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                //---print out the content of the json feed---
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    //Toast.makeText(Main2Activity.this,arrayProduct.size()+""+jsonObject.getString("Name"),Toast.LENGTH_LONG).show();
                    Product p=new Product();
                    productBrand pb=new productBrand();
                    productCategory pc=new productCategory();
                    p.setID(jsonObject.getInt("ID"));
                    p.setName(jsonObject.getString("Name"));
                    p.setImageProduct(jsonObject.getString("Image"));
                    p.setQuantity(jsonObject.getInt("Quantity"));
                    JSONObject jsonBrand=jsonObject.getJSONObject("ProductBrand");
                    pb.setID(jsonBrand.getInt("ID"));
                    pb.setName(jsonBrand.getString("Name"));
                    Log.d("pranababab",jsonBrand.getString("Name"));
                    p.setBrand(pb);
                    JSONObject jsonCategory=jsonObject.getJSONObject("ProductCategory");
                    pc.setID(jsonCategory.getInt("ID"));
                    pc.setName(jsonCategory.getString("Name"));
                    p.setCategory(pc);
                    if (jsonObject.getLong("PromotionPrice")==0){
                       p.setPromotionPrice(jsonObject.getLong("Price"));

                    }else {
                        p.setPromotionPrice(jsonObject.getLong("PromotionPrice"));
                    }
                   // p.setPromotionPrice(jsonObject.getLong("PromotionPrice"));
                    p.setPrice(jsonObject.getLong("Price"));
                    p.setHotFlag(Boolean.parseBoolean(jsonObject.getString("HotFlag").toString()));
                    if (jsonObject.getString("HomeFlag")=="true"){
                        arrayProduct.add(p);
                    }
                    pro.add(p);
                    //productCat.add(p);
                }adapter.notifyDataSetChanged();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private class ReadJSONBrand extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return read.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                Log.i("JSON", "Number of surveys in feed: " + jsonArray.length());

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    //Toast.makeText(Main2Activity.this,arrayProduct.size()+""+jsonObject.getString("Name"),Toast.LENGTH_LONG).show();
                    productBrand p=new productBrand();
                    p.setID(jsonObject.getInt("ID"));
                    p.setName(jsonObject.getString("Name"));
                    arrproductBrand.add(p);
                    arrayListSpBrand.add(p.getName());


                }adapterSpBrand.notifyDataSetChanged();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private class ReadJSONCategory extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return read.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                Log.i("JSON", "Number of surveys in feed: " + jsonArray.length());

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    //Toast.makeText(Main2Activity.this,arrayProduct.size()+""+jsonObject.getString("Name"),Toast.LENGTH_LONG).show();
                    productCategory p=new productCategory();
                    p.setID((jsonObject.getInt("ID")+1)*11);
                    p.setName(jsonObject.getString("Name"));
                    categoryArrayList.add(p);
                    topChannelMenu.add(0,(jsonObject.getInt("ID")+1)*11,0,jsonObject.getString("Name"));
                    mDrawerList.invalidate();
                    c=i+1;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class ReadJSONSearch extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return read3.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {
            try {
                JSONArray jsonArray = new JSONArray(result);
                for (int i=0;i<productSearch.size();i++){
                    productSearch.remove(i);
                    productSearch.clear();
                    adapter.notifyDataSetChanged();
                }
                for (int i=0;i<arrayProduct.size();i++){
                    delete(i);
                }
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    //Toast.makeText(Main2Activity.this,arrayProduct.size()+""+jsonObject.getString("Name"),Toast.LENGTH_LONG).show();
                    Product p=new Product();
                    p.setID(jsonObject.getInt("ID"));
                    p.setName(jsonObject.getString("Name"));
                    p.setImageProduct(jsonObject.getString("Image"));
                    p.setQuantity(jsonObject.getInt("Quantity"));
                    if (jsonObject.getLong("PromotionPrice")==0){
                        p.setPromotionPrice(jsonObject.getLong("Price"));

                    }else {
                        p.setPromotionPrice(jsonObject.getLong("PromotionPrice"));
                    }
                    p.setPrice(jsonObject.getLong("Price"));
                    productSearch.add(p);

                }//adapter.notifyDataSetChanged();
                for (int j=0;j<productSearch.size();j++){
                    arrayProduct.add(productSearch.get(j));
                }
                if (productSearch.size()==0){
                    Toast.makeText(Main2Activity.this,"Không có sản phẩm cần tìm!",Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public int SortASC(ArrayList<Product>arr,int n){
        for(int i=0;i<n-1;i++)
            for(int j=i+1;j<n;j++){
                if(arr.get(i).getPrice()<arr.get(j).getPrice()) {
                    product=arr.get(i);
                    arr.set(i,arr.get(j));
                    arr.set(j,product);
                }
            }

        return 1;
    }
    public void SortDECS(ArrayList<Product>arr,int n){
        for(int i=0;i<n-1;i++)
            for(int j=i+1;j<n;j++)
                if(arr.get(i).getPrice()>arr.get(j).getPrice())
                {
                    product=arr.get(i);
                    arr.set(i,arr.get(j));
                    arr.set(j,product);

                }
    }
    public void spinnerBrand(int position){

        if (position==0){
            for (int i=0;i<pro.size();i++){
                arrayProduct.add(pro.get(i));
                adapter.notifyDataSetChanged();
            }
        }
        if (position!=0){
            for (int i=0;i<arrayProduct.size();i++) {
                delete(i);
            }//Toast.makeText(Main2Activity.this,arrayProduct.size()+"",Toast.LENGTH_LONG).show();
            if (arrayProduct.size()==0){
                for (int i=0;i<pro.size();i++){
                    productBrand p=pro.get(i).getBrand();
                    //Toast.makeText(Main2Activity.this,pro.get(i).getBrand()+"",Toast.LENGTH_LONG).show();
                    if (p.getName().equals(arrayListSpBrand.get(position))){
                        arrayProduct.add(pro.get(i));
                        adapter.notifyDataSetChanged();
                    }

                }
                if (arrayProduct.size()==0){
                    //position=0;
                    AlertDialog.Builder b=new AlertDialog.Builder(Main2Activity.this);
                    b.setMessage("Không tìm thấy sản phẩm của hãng bạn vừa chọn. Vui lòng quay về và chọn hãng khác");
                    b.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();

                        }
                    });
                    b.create().show();
                    for (int i=0;i<arrayProduct.size();i++) {
                        delete(i);
                    }
                }
            }

        }
    }
    public void spinnerRankByPrice(int position){

        if (position==1){
            if (arrayProduct.size()!=0){
                SortASC(arrayProduct,arrayProduct.size());
                adapter.notifyDataSetChanged();
            }
        }
        if (position==2){
            if (arrayProduct.size()!=0){
                SortDECS(arrayProduct,arrayProduct.size());
                adapter.notifyDataSetChanged();
                //Toast.makeText(Main2Activity.this,arrayProduct.size()+"",Toast.LENGTH_LONG).show();
            }

        }
    }
    class readJSONPostCategory extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... params) {
            return read1.readJSONFeed(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                JSONArray jsonArray=new JSONArray(s);
                for (int i=0;i<jsonArray.length();i++){
                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                    PostCategory p=new PostCategory();
                    p.setID(jsonObject.getInt("ID"));
                    p.setName(jsonObject.getString("Name"));
                    p.setDescription(jsonObject.getString("Description"));
                    topChannelMenuPost.add(0,(jsonObject.getInt("ID")+1)*10,0,jsonObject.getString("Name"));
                    mDrawerList.invalidate();
                    arr1.add(p);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }
    class readJSONPost extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... params) {
            return read2.readJSONFeed(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                JSONArray jsonArray=new JSONArray(s);
                for (int i=0;i<jsonArray.length();i++){
                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                    Post p=new Post();
                    p.setID(jsonObject.getInt("ID"));
                    p.setName(jsonObject.getString("Name"));
                    p.setImage(jsonObject.getString("Image"));
                    JSONObject jsonObject1=jsonObject.getJSONObject("PostCategory");
                    PostCategory pc=new PostCategory();
                    pc.setID(jsonObject1.getInt("ID"));
                    pc.setName(jsonObject1.getString("Name"));
                    p.setPostCategory(pc);
                    p.setDescription(jsonObject.getString("Description"));
                    p.setContent(jsonObject.getString("Content"));
                    arr.add(p);
                }adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }
    public void delete(int i){
        arrayProduct.remove(i);
        arrayProduct.clear();
        adapter.notifyDataSetChanged();
    }


}
