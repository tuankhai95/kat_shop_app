package dtta.rubydang.kap;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import class_kat.Post;
import class_kat.SystemPath;
import class_kat.User;
import class_kat.readJSON;

public class LoginActivity extends AppCompatActivity {

    ImageButton imgBack;
    Button btnLogin;
    EditText edtUserName,edtpassWord;
    TextView txtRegister;
    static CheckBox chk;
    readJSON read=new readJSON();
    static String prefname="my_data1";
    static String prefnameTemp="my_data2";
    static int temp=0;
    public static User user=new User();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mapping();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean k=false;
                if (chk.isChecked()==true){
                    k=chk.isChecked();
                }
                if (edtpassWord.getText().toString().equals("")||edtUserName.getText().toString().equals("")){
                    Toast.makeText(LoginActivity.this,"Phải điền đầy đủ thông tin nhé bạn!",Toast.LENGTH_LONG).show();
                }else {
                    new readJSONLogin().execute(SystemPath.url+"api/account/login?userName="
                            +edtUserName.getText()+"&password="+edtpassWord.getText()+"&rememberMe="+k);

                }

            }
        });

    }
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        //gọi hàm lưu trạng thái ở đây
        savingPreferences();
    }
    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        //gọi hàm đọc trạng thái ở đây
        restoringPreferences();
    }
    public void mapping(){
        imgBack=(ImageButton)findViewById(R.id.imageButtonBackLogin);
        btnLogin=(Button)findViewById(R.id.buttonLogin);
        edtUserName=(EditText)findViewById(R.id.editTextUserName);
        edtpassWord=(EditText)findViewById(R.id.editTextPassWord);
        chk=(CheckBox)findViewById(R.id.checkBoxRemember);
        txtRegister=(TextView)findViewById(R.id.textViewRegister);
        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent t=new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(t);
            }
        });
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    class readJSONLogin extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {
            return read.readJSONFeed1(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                JSONObject jsonObject = new JSONObject(s);
                if (!jsonObject.getString("Id").equals("")){
                    user.setID(jsonObject.getString("Id"));
                    user.setEmail(jsonObject.getString("Email").toString());
                    user.setFullName(jsonObject.getString("FullName").toString());
                    user.setUserName(jsonObject.getString("UserName").toString());
                    user.setAddress(jsonObject.getString("Address").toString());
                    user.setPhoneNumber(jsonObject.getString("PhoneNumber").toString());
                    Intent t=new Intent(LoginActivity.this,Main2Activity.class);
                    Bundle bd=new Bundle();
                    bd.putSerializable("User",user);
                    t.putExtra("Login",bd);
                    startActivity(t);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }
    public void savingPreferences(){
        SharedPreferences share=getSharedPreferences(prefname,MODE_PRIVATE);
        SharedPreferences.Editor editor=share.edit();

        if (!chk.isChecked()){
            temp=1;
            editor.putString("username",edtUserName.getText().toString());
            editor.putString("password",edtpassWord.getText().toString());
            editor.putString("Email",user.getEmail());
            editor.putString("Id",user.getID());
            editor.putString("FullName",user.getFullName());
            editor.putString("Address",user.getAddress());
            editor.putString("Phone",user.getPhoneNumber());
            editor.putBoolean("remember",chk.isChecked());

        }else {
            editor.putString("username",edtUserName.getText().toString());
            editor.putString("password",edtpassWord.getText().toString());
            editor.putString("Email",user.getEmail());
            editor.putString("Id",user.getID());
            editor.putString("FullName",user.getFullName());
            editor.putString("Address",user.getAddress());
            editor.putString("Phone",user.getPhoneNumber());
            editor.putBoolean("remember",chk.isChecked());
        }
        editor.commit();
    }
    public void restoringPreferences(){
        SharedPreferences shre=getSharedPreferences(prefname,MODE_APPEND);
        boolean read=shre.getBoolean("remember",false);
        if (read){
            edtUserName.setText(shre.getString("username",""));
            edtpassWord.setText(shre.getString("password",""));

        }
        else {
           // Toast.makeText(LoginActivity.this,"babccaa",Toast.LENGTH_LONG).show();
        }
        chk.setChecked(read);
    }

}
