package dtta.rubydang.kap;

import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import class_kat.Product;



public class CustomHomeActivity extends ArrayAdapter<Product> {
    Activity context;
    int layoutID;
    ArrayList<Product>arr;

    public CustomHomeActivity(Activity context, int layoutID,ArrayList<Product>arr) {
        super(context, layoutID,arr);
        this.context=context;
        this.layoutID=layoutID;
        this.arr=arr;
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView=context.getLayoutInflater().inflate(layoutID,null);
        ImageView img=(ImageView)convertView.findViewById(R.id.imageViewProduct);
        ImageView img2=(ImageView)convertView.findViewById(R.id.imageViewHot);
        TextView txt=(TextView)convertView.findViewById(R.id.textViewNameProduct);
        TextView txt1=(TextView)convertView.findViewById(R.id.textViewPriceProduct);
        TextView txt2=(TextView)convertView.findViewById(R.id.textViewRuducePriceProduct);
        Product p=arr.get(position);
        txt.setText(p.getName());
        txt1.setText(p.getPromotionPrice()+" VND");
        if (p.getPromotionPrice()==p.getPrice()){
            txt2.setText("");
        }else  txt2.setText(p.getPrice()+" VND");

        if (p.isHotFlag()==true){
            img2.setImageResource(R.drawable.hot);
        }
        img.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        Picasso.with(context).load(p.getImageProduct()).into(img);

        return convertView;
    }
}
