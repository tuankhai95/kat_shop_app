package dtta.rubydang.kap;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import class_kat.CheckConnectInternet;
import class_kat.User;

public class MainActivity extends AppCompatActivity {
    Boolean isConnectionExist = false;
    CheckConnectInternet cd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cd = new CheckConnectInternet(getApplicationContext());
        isConnectionExist = cd.checkMobileInternetConn();
        if (isConnectionExist) {
            new CountDownTimer(1000,1000){

                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    Intent t=new Intent(MainActivity.this,Main2Activity.class);
                    startActivity(t);
                    finish();

                }
            }.start();
        } else {
            AlertDialog.Builder b=new AlertDialog.Builder(MainActivity.this);
            b.setTitle("Cảnh báo !");
            b.setMessage("Bạn vui lòng kết nối Wifi hoặc 3G để sử dụng ứng dụng!");
            b.setNegativeButton("Đóng", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            b.create().show();
        }


    }

}
