package dtta.rubydang.kap;

import android.app.Activity;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import class_kat.AccessToken;
import class_kat.Feedback;
import class_kat.GetAccessToken;
import class_kat.SystemPath;
import class_kat.readJSON;

public class FeedBackActivity extends AppCompatActivity {

    static AccessToken accessToken = new AccessToken();
    Button btnSend, btnDelete;
    ImageButton imgBack;
    static EditText edtEmail, edtName, edtContent;
    GetAccessToken re = new GetAccessToken();
    readJSON read = new readJSON();
    String name="", message="", email="";
    Feedback fb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);
        mapping();
        new TokenGet().execute();
        handlingButton();
    }

    private void mapping() {
        btnSend = (Button) findViewById(R.id.buttonGuiFb);
        btnDelete = (Button) findViewById(R.id.buttonDelete);
        imgBack = (ImageButton) findViewById(R.id.imageButtonBackFeedBack);
        edtEmail = (EditText) findViewById(R.id.editTextEmailFeedBack);
        edtContent = (EditText) findViewById(R.id.editTextContent);
        edtName = (EditText) findViewById(R.id.editTextFullNameFeedBack);
        name = edtName.getText().toString();
        message = edtContent.getText().toString();
        email = edtEmail.getText().toString();
        fb = new Feedback();
        fb.setName(name);
        fb.setMessage(message);
        fb.setEmail(email);
        fb.setID(0);
    }

    public void handlingButton() {
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                empty();
            }
        });
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String json1 ="{\"ID\": \"0\",\"Name\":\""+edtName.getText()+"\",\"Email\":\""+edtEmail.getText()+"\",\"Message\":\""+edtContent.getText()+"\"}";
                if (edtName.getText().toString().equals("")||edtEmail.getText().toString().equals("")||edtContent.getText().toString().equals("")){
                    Toast.makeText(FeedBackActivity.this,"Không được truyền giá trị rỗng nè!",Toast.LENGTH_LONG).show();
                }
                else {
                    new HttpAsyncTask().execute(json1);
                    Toast.makeText(getBaseContext(),"Cảm ơn bạn đã góp ý với chúng tôi !", Toast.LENGTH_LONG).show();
                    empty();
                }


            }
        });
    }

    public void empty(){
        edtName.setText("");
        edtContent.setText("");
        edtEmail.setText("");
    }
    private class TokenGet extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            return re.getToken();
        }

        @Override
        protected void onPostExecute(String s) {


            try {
                JSONObject object = new JSONObject(s);
                accessToken.setAccess_token(object.getString("access_token"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("tokentest", accessToken.getAccess_token());
            super.onPostExecute(s);
        }
    }


    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return read.postData("feedback","sendfeedback","feedbackViewModel",urls[0],accessToken.getAccess_token());
           // return  postData(urls[0]);
        }
        @Override
        protected void onPostExecute(String result) {
            //Toast.makeText(getBaseContext(),"rất cảm ơn bạn đã góp ý với chúng tôi !", Toast.LENGTH_LONG).show();
        }
    }

//send data
    private String postData(String json1) {
        HttpParams httpParameters = new BasicHttpParams();
        HttpProtocolParams.setContentCharset(httpParameters, HTTP.UTF_8);
        HttpProtocolParams.setHttpElementCharset(httpParameters, HTTP.UTF_8);
        HttpClient httpclient = new DefaultHttpClient(httpParameters);
        StringBuilder stringBuilder = new StringBuilder();
        try {
            Uri.Builder builder = new Uri.Builder();

            builder.scheme("http")
                    .authority(SystemPath.urlNotHttp)
                    .appendPath("api")
                    .appendPath("feedback")
                    .appendPath("sendfeedback")
                    .appendQueryParameter("feedbackViewModel", json1)
                    .build();

            String myUrl = builder.toString();
            Log.d("url=>",myUrl);
            HttpPost httppost = new HttpPost(myUrl);

            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(8);
            //nameValuePairs.add(new BasicNameValuePair("name", name));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
            httppost.setHeader("Authorization","bearer "+accessToken.getAccess_token());
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();
            InputStream content = resEntity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(content));
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            Log.d("RESPONSE1",stringBuilder.toString());

        }
        catch(Exception e)
        {
            Log.e("log_tag1", "Error:  "+e.toString());
        }
        return stringBuilder.toString();

    }

}