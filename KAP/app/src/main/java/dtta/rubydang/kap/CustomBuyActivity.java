package dtta.rubydang.kap;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import class_kat.OrderDetail;
import class_kat.Product;
import class_kat.SQLite;

public class CustomBuyActivity extends ArrayAdapter<OrderDetail> {
    Activity context;
    int layoutId;
    ArrayList<OrderDetail>arr;
    static int []x={1};
    public CustomBuyActivity(Activity context1, int layoutId, ArrayList<OrderDetail> arr) {
        super(context1, layoutId,arr);
        this.context = context1;
        this.layoutId = layoutId;
        this.arr = arr;
    }
    public int getCount() {
        return arr.size();
    }

    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView=context.getLayoutInflater().inflate(layoutId,null);
        TextView txt=(TextView)convertView.findViewById(R.id.textViewBuyName);
        TextView txt1=(TextView)convertView.findViewById(R.id.textViewBuyPrice);
        final TextView txt2=(TextView)convertView.findViewById(R.id.textViewBuyQuantity);
        ImageView img=(ImageView)convertView.findViewById(R.id.imageViewImageBuy);
        Button btnDe=(Button)convertView.findViewById(R.id.buttonDelete);
        final SQLite db=new SQLite(context,"cart.sqlite",null,1);
        final OrderDetail o=arr.get(position);
        final Product k=o.getProduct();
        txt.setText(k.getName());
        txt1.setText(k.getPrice()+" VND");
        txt2.setText(o.getQuantity()+"");
        //x[0]=o.getQuantity();
        Picasso.with(context).load(k.getImageProduct()).into(img);
        Button btn=(Button)convertView.findViewById(R.id.buttonBuyASC);
        Button btn1=(Button)convertView.findViewById(R.id.buttonBuyDESC);
        //final int []x={1};
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(BuyActivity.this,"abc",Toast.LENGTH_LONG).show();
                if (o.getQuantity()>1){
                    x[0]-=1;
                    if (x[0]<1)
                        x[0]=1;
                    o.setQuantity(x[0]);
                    o.getQuantity();
                    txt2.setText(x[0]+"");
                    BuyActivity.total+=-k.getPrice();
                    BuyActivity.txtTotal.setText(BuyActivity.total+" VND");
                    notifyDataSetChanged();
                }

            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (o.getQuantity()<k.getQuantity()){
                    x[0]+=1;
                    o.setQuantity(x[0]);
                    o.getQuantity();
                    txt2.setText(x[0]+"");
                    BuyActivity.total+=k.getPrice();
                    BuyActivity.txtTotal.setText(BuyActivity.total+" VND");
                    notifyDataSetChanged();
                }
            }
        });

        btnDe.setTag(position);
        btnDe.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                arr.remove(position);
                BuyActivity.delete=position;
                if (arr.size()==0){
                    BuyActivity.total=0;
                    BuyActivity.txtTotal.setText(BuyActivity.total+" VND");
                }else {
                    BuyActivity.total-=k.getPrice()*o.getQuantity();
                    BuyActivity.txtTotal.setText(BuyActivity.total+" VND");
                }
                db.QueryData("delete from Product where IdProduct="+k.getID());
                notifyDataSetChanged();
            }
        });


        return convertView;
    }


    
}

