package dtta.rubydang.kap;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class DetailImageActivity extends AppCompatActivity {

    ImageView img;
    ImageButton imgbtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_image);
        Bundle bd=getIntent().getExtras();
        String s=bd.getString("Image");
        img=(ImageView)findViewById(R.id.imageViewDetailImage);
        imgbtn=(ImageButton)findViewById(R.id.imageButtonDelete);
        Picasso.with(DetailImageActivity.this).load(s).into(img);
        imgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
