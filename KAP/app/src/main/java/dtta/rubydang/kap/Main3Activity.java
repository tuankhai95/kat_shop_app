package dtta.rubydang.kap;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import class_kat.AccessToken;
import class_kat.GetAccessToken;
import class_kat.OrderDetail;
import class_kat.Product;
import class_kat.SQLite;
import class_kat.readJSON;

public class Main3Activity extends AppCompatActivity {

    ImageButton img;
    EditText edtFullName,edtPhone,edtEmail,edtAddress,edtMessa,edtPayAddress,edtUser;
    Button btn;
    RadioButton rdTienMat,rdChuyenKhoan;
    RadioGroup rd;
    ArrayList<OrderDetail> arr1;
    readJSON read=new readJSON();
    GetAccessToken ac=new GetAccessToken();
    AccessToken accessToken=new AccessToken();
    long total;
    String order="",idCustomer="";
    String payment="";
    String name,phone,mail,address,message,payaddress;
    ArrayList<String>arrayList=new ArrayList<String>();
    final SQLite db=new SQLite(Main3Activity.this,"cart.sqlite",null,1);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        img=(ImageButton)findViewById(R.id.imageButtonBackCart);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mapping();
        process();
        Bundle bd=getIntent().getBundleExtra("OrderDetail");
        arr1=(ArrayList<OrderDetail>)bd.getSerializable("Detail");

        new TokenGet().execute();
        for (int i=0;i<arr1.size();i++){
            long m;
            Product p=arr1.get(i).getProduct();
            if (p.getPromotionPrice()!=0){
                total+=p.getPromotionPrice()*arr1.get(i).getQuantity();
                m=p.getPromotionPrice();
            }else {
                total+=p.getPrice()*arr1.get(i).getQuantity();
                m=p.getPrice();
            }

            arrayList.add("{\"ProductID\":\""+arr1.get(i).getProduct().getID()+"\",\"Quantity\":\""+
                    arr1.get(i).getQuantity()+"\",\"Price\":\""+m+"\"}");
        }
        for (int j=0;j<arrayList.size();j++){
            if (j==arrayList.size()-1){
                order+=arrayList.get(j);
            }
            else {
                order+=arrayList.get(j)+",";
            }

        }

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rdTienMat.isChecked()){
                    payment=rdTienMat.getText().toString();
                }else {
                    payment=rdChuyenKhoan.getText().toString();
                }
                name=edtFullName.getText().toString();
                phone=edtPhone.getText().toString();
                mail=edtEmail.getText().toString();
                address=edtAddress.getText().toString();
                message=edtMessa.getText().toString();
                payaddress=edtPayAddress.getText().toString();
                if(name.equals("")||phone.equals("")||mail.equals("")||address.equals("")||payaddress.equals("")||message.equals("")){
                    Toast.makeText(Main3Activity.this,"Phải nhập đầy đủ thông tin nhé bạn!",Toast.LENGTH_LONG).show();
                }else {
                    String json="{\"CustomerName\":\""+edtFullName.getText()+"\",\"CustomerBillingAddress\":\""+edtAddress.getText()+"\"," +
                            "\"CustomerEmail\":\""+edtEmail.getText()+"\",\"CustomerMobile\":\""+edtPhone.getText()+"\",\"CustomerMessage\":\""
                            +edtMessa.getText()+"\",\"PaymentMethod\":\""+payment+"\",\"CreatedBy\":\""+edtUser.getText().toString()+"\",\"CustomerShippingAddress\":\""+edtPayAddress.getText()
                            +"\",\"CustomerId\":\""+idCustomer+"\",\"SubTotal\":\"" +total+"\",\"OrderDetails\":[" +order+"]}";
                    new JSON().execute(json);
                    Log.d("totaljson1",json);
                }
                edtAddress.setText("");
                edtFullName.setText("");
                edtEmail.setText("");
                edtMessa.setText("");
                edtPayAddress.setText("");
                edtPhone.setText("");
                db.QueryData("delete from Product");
                AlertDialog.Builder b=new AlertDialog.Builder(Main3Activity.this);
                b.setTitle("Bạn có muốn qua về trang chủ không!");
                b.setMessage("Chúng tôi đã nhận được đơn hàng của bạn chúng tôi sẽ giao hàng trong thời gian sớm nhất cho bạn!");
                b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent t=new Intent(Main3Activity.this,Main2Activity.class);
                        startActivity(t);
                    }
                });
                b.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                b.create().show();

            }
        });
    }
    class JSON extends AsyncTask<String,Integer,String> {

        @Override
        protected String doInBackground(String... params) {
            return read.postData("order","create","orderViewModel",params[0],accessToken.getAccess_token());
        }

        @Override
        protected void onPostExecute(String s) {
            Log.d("ThanhCong",s);

            super.onPostExecute(s);
        }
    }

    private void mapping() {
        edtFullName=(EditText)findViewById(R.id.editTextPayFullName1);
        edtPhone=(EditText)findViewById(R.id.editTextPayPhone1);
        edtEmail=(EditText)findViewById(R.id.editTextPayEmail1);
        edtAddress=(EditText)findViewById(R.id.editTextAddress1);
        btn=(Button)findViewById(R.id.buttonDacMua1);
        edtMessa=(EditText)findViewById(R.id.editTextMessage1);
        rdChuyenKhoan=(RadioButton)findViewById(R.id.radioButtonChuyenKhoan1);
        rdTienMat=(RadioButton)findViewById(R.id.radioButtonTienMat1);
        edtPayAddress=(EditText)findViewById(R.id.editTextPayAddress1);
        rd=(RadioGroup)findViewById(R.id.radioGroup1);
        edtUser=(EditText)findViewById(R.id.editTextPayUserName);
        int id=rd.getCheckedRadioButtonId();
        switch (id){
            case R.id.radioButtonChuyenKhoan1:{
                rdTienMat.setChecked(false);
                break;
            }
            case R.id.radioButtonTienMat1:{
                rdChuyenKhoan.setChecked(false);
                break;
            }
        }

    }
    private class TokenGet extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            return ac.getToken();
        }

        @Override
        protected void onPostExecute(String s) {


            try {
                JSONObject object = new JSONObject(s);
                accessToken.setAccess_token(object.getString("access_token"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("tokentest", accessToken.getAccess_token());
            super.onPostExecute(s);
        }
    }
    public void process(){

        SharedPreferences shre=getSharedPreferences(LoginActivity.prefname,MODE_APPEND);
            idCustomer=shre.getString("Id", "").toString();
            edtUser.setText(shre.getString("username", "")+"");
            edtAddress.setText(shre.getString("Address", "")+"");
            edtEmail.setText(shre.getString("Email", "")+"");
            edtFullName.setText(shre.getString("FullName", "")+"");
            edtPhone.setText(shre.getString("Phone", "")+"");




    }

}
