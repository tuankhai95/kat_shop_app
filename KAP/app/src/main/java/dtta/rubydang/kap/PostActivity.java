package dtta.rubydang.kap;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import class_kat.Post;
import class_kat.PostCategory;
import class_kat.SystemPath;
import class_kat.readJSON;

public class PostActivity extends AppCompatActivity {

    ImageButton img;
    ArrayList<Post>arr=new ArrayList<Post>();

    CustomPostActivity adapter;
    ListView lv;
    TextView txt;
    readJSON read=new readJSON();
    readJSON read1=new readJSON();
    ArrayList<PostCategory>arr1=new ArrayList<PostCategory>();
    int s,x;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        img=(ImageButton)findViewById(R.id.imageButtonBackPost);
        lv=(ListView)findViewById(R.id.listViewPost);
        txt=(TextView)findViewById(R.id.textViewPostCategory);
        adapter=new CustomPostActivity(PostActivity.this,R.layout.activity_custom_post,arr);
        Bundle bd=getIntent().getExtras();
        ///String s=bd.getString("tintuc");
        s=bd.getInt("tintuc");
        txt.setText(s+"");
        x=s;
        //Toast.makeText(PostActivity.this,s+"",Toast.LENGTH_LONG).show();
        lv.setAdapter(adapter);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        new readJSONPostCategory().execute(SystemPath.url+"api/post/getallparents");
        new readJSONPost().execute(SystemPath.url+"api/post/getallparents");
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent t=new Intent(PostActivity.this,DetailPostActivity.class);
                Bundle b=new Bundle();
                Post p=arr.get(position);
                b.putSerializable("Post",p);
                t.putExtra("DetailPost",b);
                startActivity(t);
            }
        });

    }
    class readJSONPost extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... params) {
            return read.readJSONFeed(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                JSONArray jsonArray=new JSONArray(s);
                for (int i=0;i<jsonArray.length();i++){
                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                    Post p=new Post();
                    p.setID(jsonObject.getInt("ID"));
                    p.setName(jsonObject.getString("Name"));
                    p.setDescription(jsonObject.getString("Description"));
                    p.setImage(jsonObject.getString("Image"));
                    p.setContent(jsonObject.getString("Content"));
                    JSONObject json1=jsonObject.getJSONObject("PostCategory");
                    if (json1.getInt("ID")==x){
                        txt.setText(json1.getString("Name"));
                        arr.add(p);
                    }
                }adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }
    class readJSONPostCategory extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... params) {

            return read1.readJSONFeed(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                JSONArray jsonArray=new JSONArray(s);
                for (int i=0;i<jsonArray.length();i++){
                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                    PostCategory p=new PostCategory();
                    p.setID(jsonObject.getInt("ID"));
                    p.setName(jsonObject.getString("Name"));
                    p.setDescription(jsonObject.getString("Description"));
                    arr1.add(p);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }

}
