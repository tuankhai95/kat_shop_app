package dtta.rubydang.kap;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import class_kat.Post;

public class CustomPostActivity extends ArrayAdapter<Post> {

    Activity context;
    int layOutID;
    ArrayList<Post>arr;

    public CustomPostActivity(Activity context, int layOutID, ArrayList<Post> arr) {
        super(context, layOutID,arr);
        this.context = context;
        this.layOutID = layOutID;
        this.arr = arr;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView=context.getLayoutInflater().inflate(layOutID,null);
        TextView txt=(TextView)convertView.findViewById(R.id.textViewTittlePost);
        TextView txt1=(TextView)convertView.findViewById(R.id.textViewPostDescription);
        ImageView img=(ImageView)convertView.findViewById(R.id.imageViewPost);
        Post p=arr.get(position);
        txt.setText(p.getName());
        txt1.setText(p.getDescription());
        img.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Picasso.with(context).load(p.getImage()).into(img);
        return convertView;
    }
}
