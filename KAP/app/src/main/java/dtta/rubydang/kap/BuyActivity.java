package dtta.rubydang.kap;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Date;

import class_kat.OrderDetail;
import class_kat.Product;
import class_kat.SQLite;
import class_kat.ServiceGenerator;
import class_kat.User;
import class_kat.UserService;

public class BuyActivity extends AppCompatActivity {

    ImageButton imgBack;
    ListView lv;
    Button btnPay,btnNextbyProduct;
    static TextView txtTotal;
    static ArrayList<OrderDetail>arr=new ArrayList<OrderDetail>();
//    ArrayList<String>arr1=new ArrayList<String>();
//    ArrayAdapter<String>adapter1;
    public static int delete=-1;
    static CustomBuyActivity adapter;
    static long total=0;
    Product p;
    public SQLite db=new SQLite(this,"cart.sqlite",null,1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy);
        Bundle bd = null;

        if (getIntent().getBundleExtra("product")!=null){
            bd=getIntent().getBundleExtra("product");
            p=(Product)bd.getSerializable("productbuy");
        }

        db.QueryData("CREATE TABLE IF NOT EXISTS Product(ID integer primary key autoincrement,IdProduct integer unique,"+
                "Name varchar,Image varchar,Quantity integer,PromotionPrice varchar,Price varchar)");
        mapping();
        adapter.notifyDataSetChanged();
        if (!p.getImageProduct().equals("")){
            Cursor c=db.getData("select * from Product");
            String x="";
            while(c.moveToNext()){
                if (p.getID()==c.getInt(1)){
                    x+="1";
                }
            }
            if (x.equals("")){
                db.QueryData("insert into Product values(null,"+p.getID()+",'"+p.getName()+"','"+p.getImageProduct()+"',"
                        +p.getQuantity()+",'"+p.getPromotionPrice()+"','"+p.getPrice()+"')");
                OrderDetail o=new OrderDetail();
                o.setQuantity(1);
                o.setProduct(p);
                total+=Long.parseLong(p.getPrice()*o.getQuantity()+"");
                txtTotal.setText(total+" VND");
                arr.add(o);
               adapter.notifyDataSetChanged();
            }

            //Toast.makeText(BuyActivity.this,c.getCount()+"",Toast.LENGTH_LONG).show();

        }else {
            if (arr.size()==0)
            Data();
            txtTotal.setText(total+" VND");
        }


        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (arr.size()==0){
                    Toast.makeText(BuyActivity.this,"Chưa có sản phẩm trong giỏ hàng!",Toast.LENGTH_LONG).show();
                }else {
                    Intent t=new Intent(BuyActivity.this,PayActivity.class);
                    Bundle d=new Bundle();
                    d.putSerializable("Detail",arr);
                    t.putExtra("OrderDetail",d);
                    startActivity(t);
                    finish();
                }

            }
        });

    }
    public void mapping(){
        imgBack=(ImageButton)findViewById(R.id.imageButtonBackBuy);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        lv=(ListView)findViewById(R.id.listViewBuy);
        adapter=new CustomBuyActivity(BuyActivity.this,R.layout.activity_custom_buy,arr);
        lv.setAdapter(adapter);
        btnNextbyProduct=(Button)findViewById(R.id.buttonNextBuyProduct);
        btnPay=(Button)findViewById(R.id.buttonPay);
        txtTotal=(TextView)findViewById(R.id.textViewTotal);

        btnNextbyProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent t=new Intent(BuyActivity.this,Main2Activity.class);
                startActivity(t);
            }
        });


    }

    public void Data(){
        Cursor c=db.getData("select * from Product");
        c.moveToFirst();
        //Toast.makeText(BuyActivity.this,c.getCount()+"",Toast.LENGTH_LONG).show();
        for (int i=0;i<c.getCount();i++){
            OrderDetail o=new OrderDetail();
            Product p=new Product();
            p.setID(c.getInt(1));
            p.setName(c.getString(2));
            p.setImageProduct(c.getString(3));
            p.setQuantity(Integer.parseInt(c.getString(4)));
            p.setPrice(Long.parseLong(c.getString(6)));
            o.setProduct(p);
            o.setQuantity(1);
            total+=Long.parseLong(c.getString(6))*o.getQuantity();
            arr.add(o);
            c.moveToNext();

        }

        txtTotal.setText(total+" VND");
        adapter.notifyDataSetChanged();

    }

}
