package dtta.rubydang.kap;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import class_kat.CustomMyOrders;

public class CustomMyOrdersActivity extends ArrayAdapter<CustomMyOrders> {
    Activity context;
    int idLayout;
    ArrayList<CustomMyOrders>arrayList;
    public CustomMyOrdersActivity(Activity context,int idLayout,ArrayList<CustomMyOrders>arrayList){
        super(context,idLayout,arrayList);
        this.context=context;
        this.idLayout=idLayout;
        this.arrayList=arrayList;

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView=context.getLayoutInflater().inflate(idLayout,null);
        TextView textViewIdOrder=(TextView)convertView.findViewById(R.id.textViewIdOrder);
        TextView textViewDateOrder=(TextView)convertView.findViewById(R.id.textViewDateOrder);
        TextView textViewStatus=(TextView)convertView.findViewById(R.id.textViewStatus);
        CustomMyOrders customMyOrders=arrayList.get(position);
        textViewDateOrder.setText(customMyOrders.getDate()+"");
        textViewStatus.setText(customMyOrders.getStatus()+"");
        textViewIdOrder.setText(customMyOrders.getId()+"");
        return convertView;
    }
}
