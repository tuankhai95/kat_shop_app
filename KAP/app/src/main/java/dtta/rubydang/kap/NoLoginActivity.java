package dtta.rubydang.kap;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

import class_kat.Order;
import class_kat.OrderDetail;
import class_kat.Product;
import class_kat.SystemPath;
import class_kat.readJSON;

public class NoLoginActivity extends AppCompatActivity {

    TextView txtFullname,txtDate,txtTotalMoney,txtStatus,txtStatusOser;
    ListView lv;
    ImageButton btn;
    readJSON read=new readJSON();
    Order o;
    ArrayList<OrderDetail>arr=new ArrayList<OrderDetail>();
    CustomListProductOrderActivity adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_login);

        mapping();
        Bundle bd=getIntent().getBundleExtra("NoLogin");
        o=(Order)bd.getSerializable("Order");

        new readJSONOrderNoLogin().execute(SystemPath.url+"api/order/getbyemailandid?email="+o.getCustomerEmail()+"&id="+o.getID());
        adapter.notifyDataSetChanged();
    }

    private void mapping() {
        txtFullname=(TextView)findViewById(R.id.textViewFullname);
        txtDate=(TextView)findViewById(R.id.textViewDate);
        txtStatus=(TextView)findViewById(R.id.textViewStutas);
        txtStatusOser=(TextView)findViewById(R.id.textViewStatusOrder);
        txtTotalMoney=(TextView)findViewById(R.id.textViewTotalMoney);
        btn=(ImageButton)findViewById(R.id.imageButtonBackOrderNoLogin);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        lv=(ListView)findViewById(R.id.listViewOrderProductNoLogin);
        adapter=new CustomListProductOrderActivity(NoLoginActivity.this,R.layout.activity_custom_list_product_order,arr);
        lv.setAdapter(adapter);
    }
    class readJSONOrderNoLogin extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {
            return read.readJSONFeed(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.toString().equals("")){
                AlertDialog.Builder b=new AlertDialog.Builder(NoLoginActivity.this);
                b.setMessage("Vui lòng kiểm tra lại mã và email bạn vừa nhập chúng tôi không tìm thấy đơn hàng nào!");
                b.setNegativeButton("Có", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent t=new Intent(NoLoginActivity.this,OrdersActivity.class);
                        startActivity(t);
                        dialog.cancel();
                    }
                });
                b.create().show();
            }
            try {//Toast.makeText(LoginActivity.this,s,Toast.LENGTH_LONG).show();
                JSONObject jsonObject = new JSONObject(s);
                txtFullname.setText("Họ và tên: "+jsonObject.getString("CustomerName"));
                String s1=jsonObject.getString("CreatedDate");
                s1=s1.replace(s1.substring(10),"");
                txtDate.setText("Ngày đặt hàng: "+s1);
                txtStatus.setText("Trạng thái đơn hàng: "+status(jsonObject.getInt("Status")));
                txtStatusOser.setText("Trạng thái thanh toán: "+PaymentStatus(jsonObject.getInt("PaymentStatus")));
                txtTotalMoney.setText("Tổng tiền: "+jsonObject.getInt("SubTotal")+"VND");
                Order o=new Order();
                o.setID(jsonObject.getInt("ID"));
                o.setCustomerEmail(jsonObject.getString("CustomerEmail"));
                o.setCustomerName(jsonObject.getString("CustomerName"));
                JSONArray jsonArray=jsonObject.getJSONArray("OrderDetails");
                for (int i=0;i<jsonArray.length();i++){
                    JSONObject object=jsonArray.getJSONObject(i);
                    OrderDetail od=new OrderDetail();
                    od.setQuantity(object.getInt("Quantity"));
                    String k=""+object.getInt("Price");
                    od.setPrice(Long.parseLong(k));
                    JSONObject object1=object.getJSONObject("Product");
                    Product p=new Product();
                    p.setName(object1.getString("Name"));
                    p.setImageProduct(object1.getString("Image"));
                    od.setProduct(p);
                    arr.add(od);
                    adapter.notifyDataSetChanged();
                }//Toast.makeText(NoLoginActivity.this,arr.get(1).getProduct().getName()+"",Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }
   static public String status(int i){
        String k="";
        switch (i){
            case 10:{
                k="Chưa xử lý";
                break;
            }
            case 20:{
                k="Đang xử lý";
                break;
            }
            case 30:{
                k="Chưa xác nhận";
                break;
            }
            case 40:{
                k="Đã xác nhận";
                break;
            }
            case 50:{
                k="Đã hoàn thành";
                break;
            }case 60:{
                k="Hủy";
                break;
            }
            case 70:{
                k="Đã gửi đi";
                break;
            }case 80:{
                k="Đã đóng gói";
                break;
            }



        }
        return k;
    }
    static public String PaymentStatus(int i){
        String k="";
        switch (i)
        {
            case 11: {
                k = "Chưa thanh toán";
                break;
            }
            case 21: {
                k = "Đã thanh toán";
                break;
            }
        }
    return k;
    }

}
