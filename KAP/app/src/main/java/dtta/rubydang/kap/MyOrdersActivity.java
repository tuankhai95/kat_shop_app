package dtta.rubydang.kap;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import class_kat.CustomMyOrders;
import class_kat.Order;
import class_kat.SystemPath;
import class_kat.User;
import class_kat.readJSON;

public class MyOrdersActivity extends AppCompatActivity {

    User u;
    readJSON read=new readJSON();
    TextView txtIdOrder,txtDateOrder,txtStatus;
    ListView lv;
    ImageButton imgbtn;
    ArrayList<CustomMyOrders>arr1=new ArrayList<CustomMyOrders>();
    ArrayList<Order>arr2=new ArrayList<Order>();
    CustomMyOrdersActivity adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        Bundle bd=getIntent().getBundleExtra("Login");
        u=(User)bd.getSerializable("User");
        mapping();

        final SharedPreferences shre=getSharedPreferences(LoginActivity.prefname,MODE_PRIVATE);
        //Toast.makeText(MyOrdersActivity.this,shre.getString("Id","").toString()+"",Toast.LENGTH_LONG).show();
        Log.d("textidcustomer",u.getID().toString());

        new readJSONMyOdrer().execute(SystemPath.url+"api/order/getbycustomer?customerId="+shre.getString("Id","").toString());
    }

    private void mapping() {
        imgbtn=(ImageButton)findViewById(R.id.imageButtonBackMyOrders);
        txtIdOrder=(TextView)findViewById(R.id.textViewIdOrder);
        txtDateOrder=(TextView)findViewById(R.id.textViewDateOrder);
        txtStatus=(TextView)findViewById(R.id.textViewStatus);
        lv=(ListView)findViewById(R.id.listViewMyOrders);
        imgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        adapter=new CustomMyOrdersActivity(MyOrdersActivity.this,R.layout.activity_custom_my_orders,arr1);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent t=new Intent(MyOrdersActivity.this,MyOdersHaveLoginActivity.class);
                Bundle bd=new Bundle();
                bd.putSerializable("MyOrder",arr2.get(position));
                t.putExtra("Order",bd);
                startActivity(t);
            }
        });
    }

    class readJSONMyOdrer extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            return read.readJSONFeed(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                JSONArray jsonArray = new JSONArray(s);
                if (jsonArray.length()==0){
                    Toast.makeText(MyOrdersActivity.this,"Bạn chưa có đơn hàng nào!",Toast.LENGTH_LONG).show();
                }
                for (int i=0;i<jsonArray.length();i++){
                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                    Order order=new Order();
                    order.setID(jsonObject.getInt("ID"));
                    order.setCustomerId(jsonObject.getString("CustomerId"));
                    order.setCustomerEmail(jsonObject.getString("CustomerEmail"));
                    order.setCreatedDate(jsonObject.getString("CreatedDate"));
                    order.setCustomerName(jsonObject.getString("CustomerName"));
                    order.setSubTotal(Long.parseLong(""+jsonObject.getInt("SubTotal")));
                    order.setPaymentMethod(NoLoginActivity.status(jsonObject.getInt("Status")));
                    order.setPaymentStatus(NoLoginActivity.PaymentStatus(jsonObject.getInt("PaymentStatus")));
                    //Toast.makeText(MyOrdersActivity.this,NoLoginActivity.PaymentStatus(jsonObject.getInt("PaymentStatus")),Toast.LENGTH_LONG).show();
                    arr2.add(order);
                    CustomMyOrders customMyOrders=new CustomMyOrders();
                    String k=jsonObject.getString("CreatedDate");
                    k=k.replace(k.substring(10),"");
                    customMyOrders.setDate(k);
                    customMyOrders.setId(jsonObject.getInt("ID")+"");
                    if (jsonObject.getInt("PaymentStatus")==21){
                        customMyOrders.setStatus("Hoàn thành");
                    }else {
                        customMyOrders.setStatus("Chưa hoàn thành");
                    }
                    arr1.add(customMyOrders);
                    //
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }adapter.notifyDataSetChanged();
            //Toast.makeText(MyOrdersActivity.this,arr1.size()+"",Toast.LENGTH_LONG).show();
            super.onPostExecute(s);
        }
    }
}
