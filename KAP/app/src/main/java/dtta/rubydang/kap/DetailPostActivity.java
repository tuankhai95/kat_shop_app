package dtta.rubydang.kap;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.Display;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.Toast;

import class_kat.Post;

public class DetailPostActivity extends AppCompatActivity {

    Post post;
    ImageButton img;
    WebView wv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_post);
        Bundle b=getIntent().getBundleExtra("DetailPost");
        post=(Post)b.getSerializable("Post");
        //Toast.makeText(DetailPostActivity.this,post.getContent(),Toast.LENGTH_LONG).show();
        img=(ImageButton)findViewById(R.id.imageButtonBackDetailPost);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        wv=(WebView)findViewById(R.id.webViewPost);
        WebSettings webSetting = wv.getSettings();
        webSetting.setBuiltInZoomControls(false);
        webSetting.setJavaScriptEnabled(true);
        //webSetting.setBlockNetworkLoads(img);
        wv.setWebViewClient(new WebViewClient());
        String data1=post.getContent();
        wv.loadDataWithBaseURL("file:///android_asset/",data1 , "text/html", "utf-8",null);
        wv.loadDataWithBaseURL(null, "<style>img{display: inline;height: auto;max-width: 100%;}a{text-decoration: none;color:black;pointer-events: none;}</style>" + post.getContent(), "text/html", "UTF-8", null);
    }

}
