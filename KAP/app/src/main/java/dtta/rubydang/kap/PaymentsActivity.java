package dtta.rubydang.kap;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import class_kat.AccessToken;
import class_kat.GetAccessToken;
import class_kat.Order;
import class_kat.OrderDetail;
import class_kat.Product;
import class_kat.SQLite;
import class_kat.readJSON;

public class PaymentsActivity extends AppCompatActivity {

    RadioButton rdchuyenkhoan,rd2;
    RadioGroup rd;
    ImageButton img;
    Button btnPay;
    TextView txt;
    Order o;
    readJSON read=new readJSON();
    GetAccessToken ac=new GetAccessToken();
    AccessToken accessToken=new AccessToken();
    ArrayList<OrderDetail> arr1=new ArrayList<OrderDetail>();
    String order="";
    String payment="";
    static String text="",temp="";
    ArrayList<String>arrayList=new ArrayList<String>();
    final SQLite db=new SQLite(PaymentsActivity.this,"cart.sqlite",null,1);
    long total;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);
        mapping();
        new TokenGet().execute();
        Bundle bd=getIntent().getBundleExtra("Payment");
        o=(Order)bd.getSerializable("pay");
        for (int i=0;i<o.getArr().size();i++){
            arr1.add(o.getArr().get(i));
        }
        //Toast.makeText(PaymentsActivity.this,arr1.size()+"",Toast.LENGTH_LONG).show();
        for (int i=0;i<arr1.size();i++){
            long m;
            Product p=arr1.get(i).getProduct();
            if (p.getPromotionPrice()!=0){
                total+=p.getPromotionPrice()*arr1.get(i).getQuantity();
                m=p.getPromotionPrice();
            }else {
                total+=p.getPrice()*arr1.get(i).getQuantity();
                m=p.getPrice();
            }

            arrayList.add("{\"ProductID\":\""+arr1.get(i).getProduct().getID()+"\",\"Quantity\":\""+
                    arr1.get(i).getQuantity()+"\",\"Price\":\""+m+"\"}");
        }
        for (int j=0;j<arrayList.size();j++){
            if (j==arrayList.size()-1){
                order+=arrayList.get(j);
            }
            else {
                order+=arrayList.get(j)+",";
            }

        }
        if (rd2.isChecked()){
            payment=rd2.getText().toString();
        }else if(rdchuyenkhoan.isChecked()) {
            payment=rdchuyenkhoan.getText().toString();
        }
        rdchuyenkhoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text="Chuyển khoản qua ngân hàng VIETCOMBANK cho chúng tôi theo thông tin:\n" +
                        "Tên ngân hàng :  Ngân hàng Ngoại Thương VN - VietcomBank - chi nhánh Cần Thơ.\n" +
                        " Chủ tài khoản: Công ty CP KAP.\n" +
                        "Số tài khoản:   044.100.061.51.56\n" +
                        "Nội dung: Mua-Tên khách hàng-Tên sản phẩm-Số điện thoại";
                txt.setText(text);
            }
        });
        rd2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text="Tại nhà nơi nhận hàng\n" +
                        "Thanh toán cho nhân viên giao hàng trực tiếp của KATSHOP.";
                txt.setText(text);
            }
        });
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String json="";

                if (o.getCustomerName().equals("")||o.getCustomerBillingAddress().equals("")||o.getCustomerEmail().equals("")||o.getCustomerMobile().equals("")||payment.equals("")||o.getCustomerShippingAddress().equals("")){
                }else {
                    json="{\"CustomerName\":\""+o.getCustomerName()+"\",\"CustomerBillingAddress\":\""+o.getCustomerBillingAddress()+"\"," +
                            "\"CustomerEmail\":\""+o.getCustomerEmail()+"\",\"CustomerMobile\":\""+o.getCustomerMobile()+"\",\"CustomerMessage\":\""
                            +o.getCustomerMessage()+"\",\"PaymentMethod\":\""+payment+"\",\"CreatedBy\":\""+o.getCreatedBy()+"\",\"CustomerShippingAddress\":\""+o.getCustomerShippingAddress()
                            +"\",\"CustomerId\":\""+o.getCustomerId()+"\",\"SubTotal\":\"" +total+"\",\"OrderDetails\":[" +order+"]}";
                   new JSON().execute(json);

                }
                Main2Activity.s=o.getCustomerName();
                Log.d("paymentCustomerShipping",payment+" - "+o.getCustomerShippingAddress()+"-"+o.getCustomerBillingAddress());
                db.QueryData("delete from Product");
                temp=LoginActivity.user.getID();
                Log.d("abcj1k11k",LoginActivity.user.getID()+"1233");
                Log.d("testJson",json);
            }
        });

    }

    private void mapping() {
        img=(ImageButton)findViewById(R.id.imageButtonBackPayment);
        txt=(TextView)findViewById(R.id.textView25) ;
        rd=(RadioGroup)findViewById(R.id.radioGroup1);
        rdchuyenkhoan=(RadioButton)findViewById(R.id.radioButton);
        rd2=(RadioButton)findViewById(R.id.radioButton2);
        btnPay=(Button)findViewById(R.id.buttonPay);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        txt.setText("Tại nhà nơi nhận hàng\n" +
                "Thanh toán cho nhân viên giao hàng trực tiếp của KATSHOP.");


    }
    class JSON extends AsyncTask<String,Integer,String> {

        @Override
        protected String doInBackground(String... params) {
            return read.postData("order","create","orderViewModel",params[0],accessToken.getAccess_token());
        }

        @Override
        protected void onPostExecute(String s) {
            Log.d("ThanhCong",s);
            try {
                JSONObject jsonObject =new JSONObject(s);

                    AlertDialog.Builder b=new AlertDialog.Builder(PaymentsActivity.this);
                    b.setTitle("Bạn có muốn qua về trang chủ không!");
                    b.setMessage("Chúng tôi đã nhận được đơn hàng của bạn chúng tôi sẽ giao hàng trong thời gian sớm nhất cho bạn!");
                    b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent t=new Intent(PaymentsActivity.this,Main2Activity.class);
                            startActivity(t);
                        }
                    });
                    b.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    b.create().show();

            } catch (JSONException e) {
                e.printStackTrace();
            }

            super.onPostExecute(s);
        }
    }
    private class TokenGet extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            return ac.getToken();
        }

        @Override
        protected void onPostExecute(String s) {


            try {
                JSONObject object = new JSONObject(s);
                accessToken.setAccess_token(object.getString("access_token"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("tokentest", accessToken.getAccess_token());
            super.onPostExecute(s);
        }
    }

}
