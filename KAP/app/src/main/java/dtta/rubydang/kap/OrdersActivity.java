package dtta.rubydang.kap;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import class_kat.Order;

public class OrdersActivity extends AppCompatActivity {

    EditText edtEmail,edtID;
    Button btnLogin,btnNext;
    ImageButton img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        mapping();
    }

    private void mapping() {
        edtEmail=(EditText)findViewById(R.id.editTextEmailOder);
        edtID=(EditText)findViewById(R.id.editTextIDOrder);
        btnLogin=(Button)findViewById(R.id.buttonOrderLogin);
        btnNext=(Button)findViewById(R.id.buttonNext);
        img=(ImageButton)findViewById(R.id.imageButtonBackOrder);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent t=new Intent(OrdersActivity.this,LoginActivity.class);
                startActivity(t);
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtID.getText().toString().equals("")||edtEmail.getText().toString().equals("")){
                    AlertDialog.Builder b=new AlertDialog.Builder(OrdersActivity.this);
                    b.setMessage("Bạn vui lòng nhập đầy đủ thông tin");
                    b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                           dialog.cancel();
                        }
                    });
                    b.show();
                }else {
                    Order o=new Order();
                    o.setID(Integer.parseInt(edtID.getText().toString()));
                    o.setCustomerEmail(edtEmail.getText().toString());
                    Intent t=new Intent(OrdersActivity.this,NoLoginActivity.class);
                    Bundle bd=new Bundle();
                    bd.putSerializable("Order",o);
                    t.putExtra("NoLogin",bd);
                    startActivity(t);
                }

            }
        });
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
