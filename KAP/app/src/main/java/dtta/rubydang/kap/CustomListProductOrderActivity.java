package dtta.rubydang.kap;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import class_kat.Order;
import class_kat.OrderDetail;
import class_kat.Product;

public class CustomListProductOrderActivity extends ArrayAdapter<OrderDetail> {

    Activity context;
    int layOutID;
    ArrayList<OrderDetail>arr;

    public CustomListProductOrderActivity( Activity context1, int layOutID, ArrayList<OrderDetail> arr) {
        super(context1, layOutID, arr);
        this.context = context1;
        this.layOutID = layOutID;
        this.arr = arr;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView=context.getLayoutInflater().inflate(layOutID,null);
        TextView txt=(TextView)convertView.findViewById(R.id.textViewNameProductNoLogin);
        TextView txt1=(TextView)convertView.findViewById(R.id.textViewPriceProductNoLogin);
        TextView txt2=(TextView)convertView.findViewById(R.id.textViewQuantityNoLoin);
        ImageView img=(ImageView)convertView.findViewById(R.id.imageViewImage);
        OrderDetail od=arr.get(position);
        txt1.setText(od.getPrice()+" VND");
        txt2.setText("Số lượng: "+od.getQuantity()+"");
        txt.setText(od.getProduct().getName()+"");
        Picasso.with(context).load(od.getProduct().getImageProduct()).into(img);

        return convertView;
    }
}
