package dtta.rubydang.kap;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;

import class_kat.Product;

public class MoreImageActivity extends AppCompatActivity {

    ArrayList<String>arrayList=new ArrayList<String>();
    GridView gv;
    ImageButton btnBack;
    CustomMoreImageActivity adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_image);
        Bundle bd=getIntent().getBundleExtra("MoreImage");
        final ArrayList<String> arr=(ArrayList<String>)bd.getSerializable("Image");
        gv=(GridView)findViewById(R.id.gridViewMoreImage);
        btnBack=(ImageButton)findViewById(R.id.imageButtonBackMoreImage);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        adapter=new CustomMoreImageActivity(MoreImageActivity.this,R.layout.activity_custom_more_image,arr);
        gv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent t=new Intent(MoreImageActivity.this,DetailImageActivity.class);
                t.putExtra("Image",arr.get(position));
                startActivity(t);
            }
        });

    }
}
